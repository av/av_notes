**This is an old version of the notes.  See the most recent here: https://gitlab.msu.edu/av/autonomous-vehicles**

# ECE-CSE 434 Autonomous Vehicles Course: Build

Notes for: **Fall Semester, 2021**
___
### Instructor and author: 
### Daniel Morris, PhD, Associate Professor, https://www.egr.msu.edu/~dmorris

Copyright (C) 2020 - 2021
___

The documents in this repository are class notes for the **Build** portion of *ECE-CSE 434 Autonomous Vehicles*.  They will guide you through learning ROS as well as implementing some key topics in Autonomous Vehicles.  The style is hands-on, and intended for you to be following along and entering the commands onto your own computer.  I believe you'll learn best if you try out the examples as you read them.

In addition to being a tutorial, these documents are also a troubleshooting reference.  For beginners it is easy to forget or skip important steps when building or running ROS packages.  If something is not working that you think should be working, head to the appropriate section and see what you did differently.  

# 1. Setup Your Environment

The following are installation instructions to get the software you need for the class.

* **[Setup Ubuntu](Setup/Ubuntu.md)**:  A pre-configured remote Ubuntu environment with ROS
* **[ROS Packages](Setup/ROS_Packages.md)**: Additional ROS packages used in the course
* **[Python Environment](Setup/Python_Environment.md)**: Configure Python to use the ROS environment and libraries such as OpenCV
* **[VSCode](Setup/VSCode.md)**: My preferred IDE to debug Python and ROS code.  Debugging examples are included.
* **[Trouble Shooting](Setup/Trouble_Shooting.md)**: Lists common issues and solutions for those learning ROS
* **[Assignments and Git](Setup/Assignments_Git.md)**: Basics for using Git in this class, including receiving and submitting assignments

___
# 2. Python Review

* **[Python Notes Repo](https://github.com/dmorris0/python_intro/blob/main/README.md)**
  * A separate repository that provides a quick, intense review of the Python background you will need for this course.  You can optionally install Python on your own PC as described in these notes, or else use the provided VDI Remote Desktop with Python and VSCode already installed as described above.

___
# 3. ROS 1

* **1. [Start ROS](ROS/ROS_Start.md)** 
  * [ROS Prerequisites](ROS/ROS_Start.md#1-ros-prerequisites)
  * [Create a Workspace](ROS/ROS_Start.md)
  * [Create ROS Packages](ROS/ROS_Start.md)
  * [Example 1: Bare-Bones ROS Nodes](ROS/ROS_Start.md#4-example-1-bare-bones-ros-nodes)
  * [Example 2: Class-Based ROS Nodes](ROS/ROS_Start.md#5-example-2-class-based-ros-nodes)
* **2. [ROS Messaging](ROS/Messaging.md)** 
  * [ROS Messages](ROS/Messaging.md#ros-messages)
  * [ROS Topics](ROS/Messaging.md#ros-topics)
  * [Defining Your Own Message Type](ROS/Messaging.md#defining-your-own-message-type)
* **3. [Turtlebots and Gazebo](ROS/Turtlebots_Gazebo.md)** 
  * [Turtlebots as Mobile Platforms](ROS/Turtlebots_Gazebo.md#turtlebots-as-mobile-platforms)
  * [Turtlebots in Gazebo](ROS/Turtlebots_Gazebo.md#turtlebots-in-gazebo)
  * [Example Node to Drive in a Circle](ROS/Turtlebots_Gazebo.md#example-node-to-drive-in-a-circle)
  * [Real Turtlebots](ROS/Turtlebots_Gazebo.md#real-turtlebots)
* **4. [ROS with Multiple Threads and OpenCV](ROS/threads.md)**
  * [Mutli-Threaded Nodes](ROS/threads.md#multi-threaded-nodes)
  * [OpenCV in Call-Backs](ROS/threads.md#opencv-in-call-backs)
  * [OpenCV with Locks](ROS/threads.md#opencv-with-locks)
* **5. [ROS Launch and Parameters](ROS/launch.md)** 
  * [Launching Nodes](ROS/launch.md#launching-nodes)
  * [Example Launch Files](ROS/launch.md#example-launch-files)
  * [ROS Parameters](ROS/launch.md#ros-parameters)
* **6. [Coordinate Transforms](ROS/Coordinate_Transforms.md)**
  * [Coordinate Systems](ROS/Coordinate_Transforms.md#coordinate-systems)
  * [Transforming Frames with Examples](ROS/Coordinate_Transforms.md#transforming-frames-with-examples)
  * [Publishing Static Transforms](ROS/Coordinate_Transforms.md#publishing-static-transforms)

___
# 4. AV Topics
* **1. [Colored Object Detection](AV/Color_Detect.md)** 
  * [Logistic Regression](AV/Color_Detect.md#logistic-regression)
* **2. [Deep Neural Networks](AV/dnns.md)** 
  * [Inference with OpenCV](AV/dnns.md#inference-with-opencv)
  * [Integration with ROS](AV/dnns.md#integration-with-ros)
* **3. [SLAM and Navigation in the Greenline World](https://gitlab.msu.edu/av/greenline)** 
  * Installation
  * Start the World
  * Visualize and Control the Turtlebot
* **4. [State Machines](AV/State_Machines.md)** 
  * [Create `vacbot` Package](AV/State_Machines.md#create-vacbot-package)
* **5. [Tracking and Prediction](AV/Tracking.md)** 
  * [Kalman Filter](AV/Tracking.md#kalman-filter)
* **6. [PID (Proportional, Integral, Derivative) Line Follower](AV/PID.md)** 
  * [PID Controller](AV/PID.md#pid-controller)
  * [Line Follower](AV/PID.md#line-follower)

