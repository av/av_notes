# ROS Packages

### Author: Daniel Morris
### [Index](../Readme.md)
___

* [Install Additional Packages](#install-additional-packages)
* [Install Turtlebot Packages](#install-turtlebot-packages)

Starting with an full-desktop installlation of ROS either provided by the VDI ROS VM or [installed on your own](Optional/ROS.md), you will need a number of additional packages that do not come with the base ROS install.  Installation of these is described here:

# Install Additional Packages

To install additional ROS packages that do not come with the ROS, use the command:
```
$ sudo apt install ros-noetic-<package-name>
```
The course uses a number of additional packages which you should install as follows:
```
$ sudo apt update
$ sudo apt install ros-noetic-joy ros-noetic-teleop-twist-joy \
  ros-noetic-teleop-twist-keyboard ros-noetic-laser-proc \
  ros-noetic-rgbd-launch ros-noetic-rosserial-arduino \
  ros-noetic-rosserial-python ros-noetic-rosserial-client \
  ros-noetic-rosserial-msgs ros-noetic-amcl ros-noetic-map-server \
  ros-noetic-move-base ros-noetic-urdf ros-noetic-xacro \
  ros-noetic-compressed-image-transport ros-noetic-rqt-image-view \
  ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers
```

# Install Turtlebot Packages

Install pre-built Turtlebot packages with these commands:
```
$ sudo apt update
$ sudo apt install ros-noetic-turtlebot3-msgs ros-noetic-turtlebot3
```

And then install from source the Turtlebot Simulations by building it as follows:
```
$ source /opt/ros/noetic/setup.bash
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
$ cd ~/catkin_ws && catkin_make
$ source devel/setup.bash
```
Make sure the build completes successfully.  If you get a python build error, you may need to install your virtual Python environment as explained in [Python_Environment.md](Python_Environment.md).  Activate it, and then rerun the last two lines of the above.

___
### [Back to Index](../Readme.md)
