#!/usr/bin/env python
'''
    string_pub.py 

    This is a ROS node that demonstrates publishing a string

    See also: string_sub.py

    Daniel Morris, Sep 2021
'''
import rospy
from std_msgs.msg import String

class StringPub:

    def __init__(self, topic_name='greetings', node_name='message_pub'):
        rospy.init_node(node_name)      # Initialize ROS node
        self.pub = rospy.Publisher(topic_name, String, queue_size=10)
        rospy.loginfo('Publishing to topic: ' + topic_name )
        
    def run(self):
        ''' Input and publish until interrupt '''
        while not rospy.is_shutdown():  # Exit loop if Ctrl-C pressed
            pstring = input('Publish: ')
            self.pub.publish(pstring)
    
if __name__=="__main__":

    strpub = StringPub()
    strpub.run()  

