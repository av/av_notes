#!/usr/bin/env python
'''
    image_sub.py 

    This is an exmple ROS node for subscribing to image topics and displaying images
    Can specify the topic name as well as whether or not it is compressed

    Usage:
      python image_sub.py <topic_name>
      python image_sub.py <topic_name> --compressed

    Daniel Morris, Nov 2020
'''
import argparse
import rospy
import cv2
from sensor_msgs.msg import Image, CompressedImage
from cv_bridge import CvBridge, CvBridgeError

class ImageSub:
    def __init__(self, topic_name, compressed):
        self.compressed = compressed
        self.bridge = CvBridge()
        rospy.loginfo(f'Subscribing to topic: {topic_name} with compressed: {self.compressed}')
        if self.compressed:
            rospy.Subscriber(topic_name, CompressedImage, self.image_cb)
        else:
            rospy.Subscriber(topic_name, Image, self.image_cb)

    def image_cb( self, msg ):
        try:
            if self.compressed:
                img = self.bridge.compressed_imgmsg_to_cv2(msg,'bgr8')
            else:
                img = self.bridge.imgmsg_to_cv2(msg,'bgr8')
        except CvBridgeError as e:
            print(e)
        cv2.imshow('Image',img )
        if cv2.waitKey(1) & 0xFF == ord('q'):
            rospy.signal_shutdown('Quitting')

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Image type arguments')
    parser.add_argument('topic_name',      default="image",     type=str,    help="Image topic to subscribe to")
    parser.add_argument('--compressed',    action='store_true',    help='Set if compressed image')
    args, unknown = parser.parse_known_args()  # For roslaunch compatibility
    if unknown: print('Unknown args:',unknown)

    rospy.init_node('Image_Subscriber')
    ImageSub( topic_name=args.topic_name, compressed=args.compressed)  
    rospy.spin()
