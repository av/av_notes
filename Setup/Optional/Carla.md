# Carla on Remote Desktop

### Author: Daniel Morris
### Instructions adapted from DECS

### [Index](Readme.md)
___

# Carla Intro

[Carla](http://carla.org/) is an open source 3D world simulator with excellent support for automotive applications.  It includes a ROS bridge that translate Carla messages to ROS messages, and so enable ROS nodes to subscribe to sensor and other topics being published by the ego-vehicle.

For this course DECS has set up Carla on 4 GPU machines running Windows.  You will need access permissions to one of the GPU machines in order to run CARLA, and we will work out a way to provide that given needs, particularly project needs.  

# Carla Server Setup

**On the GPU machine**. Currently `eb3214p29.ad.egr.msu.edu` for instructors, but machine with permissions for students will be assigned later.  To access the computer you will need to use the DECS rdp file as explained [here](https://www.egr.msu.edu/decs/help-support/how-to/connect-office-or-research-lab-computers-using-remote-desktop-gateway).

Map the network folder `\\cifs\courses\ECE\491\Instructors\Carla Simulator` to your Z drive (or other drive).

Open a command window, type the drive name you selected `Z:` and this will take you to the Carla folder containing the `CarlaUE4.exe` file.  To start Carla type:
```
CarlaUE4
```
After a delay, it should bring up a window with a viewpoint like this:

![Carla](.Images/Carla_1.png)

There are various Python utilities in the `PythonAPI/examples` folder.  Try these using Python 3.7 (default on the computer):
```
python spawn_npc.py -n 30 -w 300 --safe
```
```
dynamic_weather.py --speed 5.0
```
You should see the weather rapidly change and various walkers and vehicles drive around:

![Carla Rain](.Images/Carla_5.png)

Note: quit these spawned movers before the next step, as you can spawn them directly from ROS over the Carla-ROS bridge.

# Carla Client & ROS Bridge

**One your VDI Desktop** 

You will be running ROS on a separate machine, most likely your VDI remote desktop that runs WSL.  On this machine you can build the `carla-ros-bridge` as explained in this document [https://github.com/carla-simulator/ros-bridge](https://github.com/carla-simulator/ros-bridge).  The `sudo apt install` is currently not available for Noetic, so use the instructions under *For Developers*, which are the following.  First make sure `rosdep` is installed with:
```
sudo apt install python3-rosdep
```
Then run the following:
```
#setup folder structure
mkdir -p ~/carla-ros-bridge/catkin_ws/src
cd ~/carla-ros-bridge
git clone https://github.com/carla-simulator/ros-bridge.git
cd ros-bridge
git submodule update --init
cd ../catkin_ws/src
ln -s ../../ros-bridge
source /opt/ros/noetic/setup.bash
cd ..

#install required ros-dependencies
rosdep update
rosdep install --from-paths src --ignore-src -r

#build
catkin_make
```

Next, you will need the `carla-0.9.10-py3.7-linux-x86_64.egg` file on your `PYTHONPATH`.  This file is available on the network drive folder: `\\cifs\courses\{ECE or CSE}\491\Shared`.  (You can map this to a drive and then access it within WSL). Copy the egg file to a location in your ubuntu filesystem and *add* the following line to your `~/.bashrc` file (*with the correct path inserted*):
```
export PYTHONPATH=$PYTHONPATH:<path_to_file>/carla-0.9.10-py3.7-linux-x86_64.egg
```
Then source your `~/.bashrc`, *and* your `carla-ros-bridge` overlay:
```
source ~/carla-ros-bridge/catkin_ws/devel/setup.bash
```
You will also need a Python environment.  I recommend the virtual environment you created which is activated with:
```
source ~/envs/work/bin/activate
```
To check that your `.egg` file is actually on your `PYTHONPATH`, you should be able to do the following with no errors:
```
$ python
>>> import carla
>>> exit()
```

There are multiple `.launch` files provided with the `carla-ros-bridge` that show examples of configuring ego-vehicles with sensors and spawning other vehicles and walkers.  You can modify these as you see fit.  For the AV project, you may wish to use the sensor configuration used by the AutoDrive team.  This is how to run one of the examples:
```
roslaunch carla_ros_bridge carla_ros_bridge_with_example_ego_vehicle.launch host:=<full_hostname_of_carla_computer>
```

Once launched, a remote operation window pops up, and you can control the ego-vehicle from this window.  Some instructions on this are here:
[https://carla.readthedocs.io/en/stable/simulator_keyboard_input/](https://carla.readthedocs.io/en/stable/simulator_keyboard_input/). Pressing `P` will toggle autopilot.

In a separate terminal you can run Rviz, and view the Lidar and other sensors.

## Synchronous Mode

Carla operates in a client-server mode; the server generating the world sensor data and the client sending requests to the server and receiving data from the server.  This interaction can operate asynchronously or synchronously, and the server can run with variable time-steps or fixed time-steps, as explained [here](https://carla.readthedocs.io/en/latest/adv_synchrony_timestep/).  For data collection it is best to use synchronous mode with fixed time-steps.  A way to achieve that is to include the following in your launch file (like the one above) on the Carla client machine:
```
<!-- Synchronous mode-->
  <arg name='synchronous_mode' default='True'/>
  <arg name='synchronous_mode_wait_for_vehicle_control_command' default='False'/>
  <arg name='fixed_delta_seconds' default=''/>
```
So edit the launch file and ensure the `synchronous_mode` option is set to `True`.  

Here is a [video example](https://mediaspace.msu.edu/id/1_3lqystyf) running the above launch file plus opening RViz and displaying the lidar, the forward camera and the semantic segmentation.

![Carla](.Images/Carla_6.png)

___
# Using Carla

We have just a few GPU machines that are available to run Carla.  They are available for reservation, up to a 12-hour period at a time: 6pm-6am, or 6am-6pm.  

## Reservation Guidelines:
* Please request only **one machine per group** for any period of time.  
* These GPU machines are **only** for running Carla.  Do not run other software on them.  
* If there is high demand for use, we may need to work out another reservation method.
* To reserve:
   * Send a request to the instructors on Piazza specifying the date and time-block (either 6pm-6am, or 6am-6pm) that you want to use a GPU machine for Carla.
   * Make your request at least 24 hours ahead of when you will want to use it.
   * Provide your MSU userid.

___
### [Back to Index](Readme.md)
