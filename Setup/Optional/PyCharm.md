# Remotely Editing with VSCode

### Author: Daniel Morris
### [Index](Readme.md)

# Introduction

If you are having difficulties with VSCode on the VDI desktop, you can use any other IDE you like. PyCharm is a popular alternative IDE.  The following is a brief set of instructions to get you going.  

# Install PyCharm

Download the `linux` version of PyCharm from this page: [https://www.jetbrains.com/pycharm/download/#section=linux](https://www.jetbrains.com/pycharm/download/#section=linux).  The Community version is free and the Professional version may be free for students.  

Open the folder in which it downloaded in Windows Explorer. You should see a pycharm `tar` file (not `exe` file which is the Windows version).  Copy it into your home folder in your Ubuntu filesystem.  Note that you can `\\wsl$` takes you to your WSL filesystem, and for this class your home folder is at `\\wsl$\ECE491Ubuntu2004\home\av\$.  

![Download](.Images/pycharm1.png)
![Download](.Images/pycharm2.png)

In your Ubuntu shell, make sure you can locate the copied `.tar.gz` file in your home folder (i.e `/home/av`):
```
$ cd
$ ls
```
Extract the software from the tar file with:
```
tar -xzf pycharm-community-2020.3.2.tar.gz
```
Then `cd` to the bin folder:
```
cd pycharm-community-2020.3.2/bin
```
Then to run PyCharm, type:
```
sh pycharm.sh
```
You should be able to open and run Python files, including ROS nodes.


___
### [Back to Index](Readme.md)
