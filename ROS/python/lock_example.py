#!/usr/bin/env python
''' Illustration of when and when not to use locks '''
from threading import Lock
import rospy
from std_msgs.msg import Int32

class Example():

    def __init__(self):
        self.a = 1              # Only accessed in main thread
        self.b = None           # Accessed in main thread and callback
        self.c = None           # Only accessed in callback
        self.lock_for_b = Lock()        
        rospy.Subscriber('mytopic',Int32,self.callback)

    def run(self):              # Only called in main thread
        self.a += 1             # No lock
        self.lock_for_b.acquire()
        print(self.b)           # Accessing shared memory
        self.lock_for_b.release()

    def callback(self, msg):    # Called in separate thread
        self.c = msg.data       # Only accessed by callback, no lock
        print(msg.data)         # msg is only accessed in callback -- no lock
        self.lock_for_b.acquire()
        self.b = msg.data       # self.b is also accessed by the main thread
        self.lock_for_b.release()


if __name__=="__main__":
    rospy.init_node('example')
    e = Example()
    rospy.sleep(5.0)
    e.run()
    