#!/usr/bin/env python
'''
    string_sub.py 

    This is a ROS node that subscribes to a string topic

    See also: string_pub.py

    Daniel Morris, Sep 2021
'''
import rospy
from std_msgs.msg import String

class StringSub:

    def __init__(self, topic_name='greetings', node_name='message_sub'):
        rospy.init_node(node_name)  # Initialize ROS node
        rospy.Subscriber(topic_name, String, self.read_callback)
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        rospy.loginfo(msg.data)

if __name__=="__main__":

    strsub = StringSub()    # Create a subscriber that reads and decodes the topic
    rospy.spin()            # Wait and perform callbacks until node quits

