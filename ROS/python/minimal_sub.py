#!/usr/bin/env python
'''
    minimal_sub.py 

    This is a minimal ROS node that subscribes to an integer-valued topic

    See also: minimal_pub.py

    Daniel Morris, Sep 2021
'''
import rospy                            # Required package for Python ROS nodes
from std_msgs.msg import Int32          # Message type subscribed
    
def read_callback( msg ):               # Define a callback
    rospy.loginfo(f'Read: {msg.data}')  # Log each message we get

if __name__=="__main__":

    rospy.init_node('Subscribe_Int32')    # Always initialize a node
    rospy.Subscriber('inc_topic', Int32, read_callback)  # Subscriber defined with topic name, message type and callback
    rospy.spin()  

