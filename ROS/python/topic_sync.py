#!/usr/bin/env python
'''
    topic_sync.py

    Example of synchronizing two topics and passing them to a single callback
'''
import cv2
import rospy
import message_filters
from sensor_msgs.msg import CompressedImage, CameraInfo
from cv_bridge import CvBridge

class TopicSync:
    
    def __init__(self):
        self.bridge = CvBridge()
        img_sub = message_filters.Subscriber('raspicam_node/image/compressed', CompressedImage)
        info_sub = message_filters.Subscriber('raspicam_node/camera_info', CameraInfo)
        self.ts = message_filters.TimeSynchronizer([img_sub, info_sub], 2)
        self.ts.registerCallback(self.show_image)        

    def show_image(self, img_msg, info_msg):
        img = self.bridge.compressed_imgmsg_to_cv2(img_msg,'bgr8')
        focal_x = info_msg.K[0]  # First element of K is x focal length
        cv2.putText(img, 'Fx: '+str(focal_x), (5,30), cv2.FONT_HERSHEY_SIMPLEX, 1, (200,0,200), 2)
        cv2.imshow("Image",img)
        if (cv2.waitKey(2) & 0xFF) == ord('q'):
            rospy.signal_shutdown('Quitting')

if __name__=="__main__":

    rospy.init_node('show_focal', anonymous=True)
    TopicSync()
    rospy.spin()
