# ROS Installation

### Author: Daniel Morris
### [Index](Readme.md)
___

* [Install ROS 1 Noetic](#install-ros-1-noetic)
* [Uninstalling ROS and Gazebo](#uninstalling-ros-and-gazebo)

# Install ROS 1 Noetic

Those using the VDI ROS VM will have ROS pre-installed, and can proceed to the next [section](#Install-Additional-Packages) for installing additional packages.

If you are starting with a running Ubuntu instance, then it is quite simple to install ROS Noetic.  Follow the instructions on this page: 
[wiki.ros.org/noetic/Installation/Ubuntu](http://wiki.ros.org/noetic/Installation/Ubuntu).  The download may take a while, and you can do other work while it installs.  Note: In section 1.4, install the `Desktop-Full` in order to get all the packages you'll need; otherwise you will need to install various ROS packages along the way.

You will find that ROS is installed into the folder `/opt/ros/<distro>`, where `<distro>` is `noetic`.  Other ROS versions are installed in their own subfolder here.  For example, the recent ROS 2 release, Foxy, would be installed in `/opt/ros/foxy`.  

___
# Uninstalling ROS and Gazebo

*This section is just FYI -- do not actually uninstall ROS.*  It is always good to know how to unintall something that you have installed. 

## Uninstall ROS
To uninstall all ROS versions:
```
$ sudo apt-get remove ros-*
```
To uninstall just ROS Noetic:
```
$ sudo apt-get remove ros-noetic*
```
Then do:
```
$ sudo apt-get autoremove
```
## Uninstall Gazebo:

Gazebo is a separate simulator, so to uninstall it you can do:

```
$ sudo apt-get remove ros-noetic-gazebo*
$ sudo apt-get remove libgazebo*
$ sudo apt-get remove gazebo*
```
___
### [Back to Index](Readme.md)
