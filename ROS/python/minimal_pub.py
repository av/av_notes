#!/usr/bin/env python
'''
    minimal_pub.py 

    This is a minimal ROS node that publishes integer values to a topic

    See also: minimal_sub.py

    Daniel Morris, Sep 2021
'''
import rospy                            # Required package for Python ROS nodes
from std_msgs.msg import Int32          # Message type that we will publish
    
if __name__=="__main__":

    rospy.init_node('Publish_Int32')    # Must always initialize a node with node name using init_node()
    rate = rospy.Rate(2)                # Will use this to limit publishing to 2 Hz   
    pub = rospy.Publisher('inc_topic', Int32, queue_size=2)  # Create publisher with topic name, type and queue size
    inc = 0
    while not rospy.is_shutdown():           # Quit on interrupt
        pub.publish(inc)                     # Publish an inteter
        rospy.loginfo(f'Publishing: {inc}')  # Optional logging
        inc += 1
        rate.sleep()                         # Ensures loop does not exceed rate set above
