# ROS in a Docker

### Author: Daniel Morris
## [Index](Readme.md)
## Contents
* [Install Docker](#install-docker)
* [Docker Images and Containers](#docker-images-and-containers)
* [ROS Development Image](#ros-development-image)
* [Create a Container](#create-a-container)
* [More Container Commands](#more-container-commands)
___


# Install Docker 

To install Docker, follow the installation instructions below depending on your environment:
* [Mac installation](https://docs.docker.com/docker-for-mac/install/)
* [Windows computers with WSL 2](https://docs.docker.com/docker-for-windows/wsl/) (this is the preferred option for Windows)
* [Windows computers](https://docs.docker.com/docker-for-windows/install/)  without WSL 2

# Docker Images and Containers

There is plenty of online documentation on Dockers images and containers.  In brief, the process of creating a Docker container is as follows.  First a Docker text file is created (such as the one I built [here](docker/noetic_full.Dockerfile)) which specifies a sequence of steps for building a Docker image (including the operating system and application software components needed).  From this Dockerfile, a read-only image is built.  The resulting Docker image can be large as it includes all the code and executables.  To make a change to an image requires rebuilding it.  Now a container is a running instance of an image.  It includes additional read/write memory that can be changed.  Containers are compute environments isolated from your system, and can be configured to pass data to and from your system if needed.  Containers are usually meant to be temporary; you delete them when you're done with them, although it is possible to keep a container around for a long time.  A container acts like a complete virtual machine (although uses fewer resources), and can support multiple shells and multiple applications, just like a virtual machine.  

# ROS Development Image

There are are many pre-built Docker images available for anyone to use.  The official ROS images hosted by Docker are here: [https://hub.docker.com/_/ros](https://hub.docker.com/_/ros), and in addition there are desktop ROS images with additional packages maintained by OSRF here: [https://hub.docker.com/r/osrf/ros/tags](https://hub.docker.com/r/osrf/ros/tags).  You can use one of these images directly to create a container, or you can use them as a building block for to create your own image, which is what we do in this class.

The official Docker images only have a `root` user and not all the packages we will need.  It is not hard to extend these images to create an Ubuntu / ROS environment that is more suitable for development.  Some key components of this are
1. Create a non-root user
2. Mount one or more folders from the host OS in the Docker container
3. Optionally use git to download and upload source code to/from the container
4. Optionally install additional apps

Item 2 is the most important.  By mounting a folder from your host OS, changes you make to this folder will persist beyond the lifetime of the container.  It is also an easy way to pass files to and from your container.  Item 3 is an alternative way to transfer source code, although not large datafiles, to and from the container.  

With this Docker file: [docker/noetic_full.Dockerfile](docker/noetic_full.Dockerfile), I created a Docker image called `morris2001/noetic_full`.  It included the above components to make it more suitable for developing ROS code in this class.  In the remainder of this document I will summarize the Docker commands needed to use this image.

# Create a Container

Before creating a container from this image, plan how you will transfer code and data to and from it.   It is best to think of a container as temporary, so important data should have a copy stored outside the container.  A good way to achieve this is to mount a folder from your host computer (Windows, Mac, Ubuntu, WSL, etc.) in your container.  Then from in your container you can copy to/from this folder, or directly use files in it.  On my system I selected a WSL folder `/home/dmorris/data` and mount it in my container at `/mnt/data`.  You could mount Windows folders too, such as `c:/Users/...`, but Docker says this is slower.  For code, cloning a Git repo in your container is a good solution for transfering files to and from your container.  I also like to mount a code folder in my WSL instance: `/home/dmorris/repos` to `/mnt/code` for easy sharing with the container.   You are free to choose whichever folders make most sense for your system.  Once you make a container and specify which folders to mount, that is permanent for that container.  If you want to change them, you'll need to delete that container and make a new one.  But that's okay, as remember, containers are meant to be temporary.  

Here is the command I use to create a container from the above ROS image:
````
docker run -it -v /home/dmorris/repos:/mnt/code -v /home/dmorris/data:/mnt/data --network=host --name noetic1 morris2001/noetic_full
````
Here the `-v <host_folder>:<container_location>` options mounts both my code folder and my data folder.  Here you will specify **your own local folders** to mount.  

The `--name <container_name>` option creates a container with my selected name: `noetic1` instead of from a randomly generated pair of words.  This makes it easier to remember which container I am working in, and easier to re-enter it from additional terminal windows.  In the following examples I will use `noetic1` as my container name, but of course when you create your container from the image you can choose any name you like.

The `--network=host` option specifies that the container shares the host ports and other network details.  For end-use security reasons this is not ideal, but here it is useful as it enables multiple Docker containers to communicate between each other via ROS. (For example, you can run `roscore` in one container, `gazebo` in another, and ROS nodes in another.)  I would have expected this would let the containers communicate with WSL, but it turns out not to be the case due to peculiarities of Docker in Windows.  That means ROS nodes in your containers will *not* communicate with nodes in WSL and vice versa.  

Here is what I get when I run the above command:

![Noetic Run](.Images/noeticRun.png)

On entering the container I have user ID `avc` and am placed in the root folder.  Changing to my home folder with `cd`, I see the `envs` folder that comes with a pre-installed Python [virtual environment](Python_Environment.md) (this was added when creating the Docker image).  And in the `/mnt` folder I see my `code` and `data` folders that give me access to folder is my Windows system.  It is not usual to have a virtual environment in a Docker container where a Python environment would have a single task.  However, here the container is intended as a ROS development environment, and so I have set it up to be just like an Ubuntu workstation.  After you create your container you should be all ready to start programming in ROS.

# More Container Commands 

Once your container is created, you can enter it whenever you like.  An easy way is to attach additional shells to it.  From another terminal window type:
````
docker exec -it noetic1 bash
````
Calling this repeatedly, you can be operate in the same container from as many windows as you wish.

If your container has stopped, you will need to start it before you can add shells, and that can be done with:
````
docker start noetic1
````

To see all your containers and their states use:
````
docker ps -a
````
![Containers](.Images/containerPS.png)

Here you can see I have two containers, called `melodic1` and `noetic1` and their status.

When you are completely finished with your container you can remove it by first stopping it:
````
docker stop noetic1
````
And then deleting it:
````
docker rm noetic1
````
Alternatively, you can run many of these commands from the Docker Desktop app (which in Windows you can start by typing `Docker` into the Windows search bar).  This brings up a window like this:

![Docker Desktop](.Images/Docker_Desktop.png)

___
### [Back to Index](Readme.md)
