#!/usr/bin/env python
''' This state machine combines the arc following state machine from
    vac_04_arc_follow.py and then drives to a waypoint using a SimpleActionState.  
    Unlike in vac_05_actions_waypoints.py, this waypoint is dynamically specified
    using a callback function to access userdata to specify the goal.

    Use example:
     roslaunch greenline greenline.launch
     roslaunch greenline mapping.launch   
     rosrun vacbot vac_06_route_follow.py  

    Note: this can be run directly after: vac_05_actions_waypoints.py

    Daniel Morris, April 2020
'''
import math
import rospy
import smach
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler
from std_msgs.msg import Header
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from vac_05_actions_waypoints import get_line_waypoints, init_follower_sm
from transform_frames import TransformFrames

    
class SetWaypointBehindWall(smach.State):
    '''This should find the wall that the robot is facing and publish a waypoint 0.5m behind it '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['set_wp'],output_keys=['out_behind_wall_goal'])
        self.trans = TransformFrames()        

    def execute(self, userdata):
        pnt = Point( 1., 0., 0.)    # Set waypoint 1m ahead of robot
        quat = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
        wp_base_footprint = Pose(pnt,quat)
        target = PoseArray(header=Header(frame_id='base_footprint', stamp=rospy.Time(0)) )
        target.poses.append( Pose(pnt,quat) )
        wp = self.trans.pose_transform(target,'map').poses[0]

        wpose = MoveBaseGoal()
        wpose.target_pose.header.frame_id = "map"
        wpose.target_pose.header.stamp = rospy.Time.now()
        wpose.target_pose.pose = wp
        userdata.out_behind_wall_goal = wpose
        return 'set_wp'

def init_circumnavigate_wall_sm():
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])
    sm.set_initial_state(['SET_POINT'])
    sm.userdata.sm_goal = MoveBaseGoal()

    with sm:  
        #First create a state to find point behind wall and store it in userdata:
        smach.StateMachine.add('SET_POINT', SetWaypointBehindWall(),
                transitions={'set_wp':'DRIVE_GOAL'},
                remapping={'out_behind_wall_goal':'sm_goal'} )
        #We will not know the position behind the wall until we get to the wall
        #So use a callback to get it from userdata
        def tbot_goal_cb(userdata, goal ):
            return userdata.behind_wall  # This can access userdata if input_keys are set in the SimpleActionState below
        smach.StateMachine.add( 'DRIVE_GOAL', 
                                SimpleActionState( 'move_base', 
                                                    MoveBaseAction,
                                                    goal_cb=tbot_goal_cb,  #set a callback
                                                    input_keys=['behind_wall'],
                                                    server_wait_timeout=rospy.Duration(10.0)),
                                transitions={'succeeded':'succeeded','preempted':'preempted','aborted':'aborted'},
                                remapping={'behind_wall':'sm_goal'} )
    return sm

def navigate_course_sm():
    '''Combines state machines into a complete state machine for the course'''
    sm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])
    sm.set_initial_state(['LINE_FOLLOWER'])
    wplist = get_line_waypoints()

    with sm: 
        smach.StateMachine.add('LINE_FOLLOWER', init_follower_sm(wplist),
                transitions={'succeeded':'WALL_CIRCUMNAV',
                             'preempted':'preempted',
                             'aborted':'aborted'})
        smach.StateMachine.add('WALL_CIRCUMNAV', init_circumnavigate_wall_sm(),
                               transitions={'succeeded':'succeeded','preempted':'preempted','aborted':'aborted'} )
    return sm
    
    
if __name__ == '__main__':
    rospy.init_node('Simplified_Course')
    sm = navigate_course_sm()
    outcome = sm.execute()
    rospy.loginfo('Final outcome: '+outcome)


