# Python Environment

### Author: Daniel Morris
### [Index](../Readme.md)
## Contents
* [Python Preliminaries](#python-preliminaries)
* [A ROS Python Environment](#a-ros-python-environment)
* [Adding Packages with a Virtual Environment](#adding-packages-with-a-virtual-environment)
  * [Create a Virtual Environment](#create-a-virtual-environment)
  * [Activate a Virtual Environment](#activate-a-virtual-environment)
  * [Add Packages to your Virtual Environment](#add-packages-to-your-virtual-environment)
  * [Quitting your Virtual Environment](#quitting-your-virtual-environment)
* [Combining the ROS Environment with your Virtual Environment](#combining-the-ros-environment-with-your-virtual-environment)
* [OpenCV](#opencv)
* [Python Resources](#python-resources)
___


# Python Preliminaries

This course will assume that you are proficient in Python.  For a quick, but intense review, see the [Python Intro Notes](https://github.com/dmorris0/python_intro).  

# Python Version for ROS and Ubuntu

ROS Noetic uses Python 3.8 and Ubuntu 20.04.  Earlier versions of ROS from Melodic and on back used Python 2.7.  You may find some incompatibilities if you run Python code written for these versions due to this.  Many of the changes are pretty superficial and are easily updated.

Now Python is pre-installed in Ubuntu 20.04, so we will not need to install it in your Ubuntu shell.  

# A ROS Python Environment

Assuming you have installed ROS Noetic in your Ubuntu environment, as explained in [ROS Installation](../Setup/Optional/ROS.md).  ROS comes with a Python 3 environment and multiple libraries including OpenCV and Matplotlib.  We will take advantage of this by using this environment.  That way there is no need to install another instance of Python or these libraries.  

Activating the ROS Python environment is very easy.  Simply source the underlay:
````
$ source /opt/ros/noetic/setup.bash
````
Confirm that you can now run `Python3` and that the packages that come with ROS are available by importing a couple of the libraries as follows:
````
$ python3
Python 3.8.10 (default, Jun  2 2021, 10:49:15)
[GCC 9.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import cv2
>>> import matplotlib
>>> exit()
````
If you get no errors you are good to go and will be able to use all the Python libraries that come with ROS.

## Create a Virtual Environment

Let's create a virtual environment named `work`.  
```bash
$ mkdir -p ~/envs
$ python3 -m venv --system-site-packages ~/envs/work
```
Here the `--system-site-packages` option makes our `work` environment an overlay by giving it access to system packages.  It will use the underlying environment and install local packages if no alternative versions are installed in the environment.  This is important because the ROS underlay creates an environment which our `work` environment can then incorporate.

Note, the first time you run `python3 -m venv` you may get an error asking you to install `python3.8-venv`, which you can
```
$ sudo apt install python3.8-venv
```
Then rerun the above command to create your virtual environment

## Activate a Virtual Environment
Now when you want to use the work environment, you can activate it with:
```bash
$ source ~/envs/work/bin/activate
```
You will see `(work)` appear at the beginning of the line showing that it is active, like this:
```bash
(work) av@ROSVM01:~$ 
```
I personally find typing this a bit tedious, so I use a bash function to activate it.  I added the following to my `~/.bashrc` file (and is part of [Ubuntu Setup](Ubuntu.md)):
```bash
act() {
    source $HOME/envs/$1/bin/activate           # activate any virtual environment in ~/envs folder
}
```
If you just added this to your `.bashrc`, then source it with:
```
$ source ~/.bashrc
```
Then to activate your `work` virtual environment simply type:
```bash
$ act work
```
If you create other environments in the `~/envs` folder, you can activate them with `act <env_name>`.

## Add Packages to your Virtual Environment
To add packages to your `work` environment, **first** activate it so that you see `(work)` at the start of your terminal line.  You can install packages with `pip3` and these will be added to this environment.  For example:
```bash
$ python -m pip install scikit-learn
```
From now on `scikit-learn` will be available when you activate `work`.

___
# Combining the ROS Environment with your Virtual Environment

As mentioned above, our virtual environment is configured to be an overlay.  To use both both the `work` environment and the ROS environment, simply activate both with:
````bash
$ source /opt/ros/noetic/setup.bash
$ source ~/envs/work/bin/activate
````
If you do this before running a ROS packages, then that package will have access to the additional packages you installed in `work`.


___
Next, install VSCode and try out the Python debugging examples in [VSCode.md](#VSCode.md).
