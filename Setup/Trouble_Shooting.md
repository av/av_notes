# Trouble Shooting

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)

There are a number of issues that people new to ROS often struggle with.  I intend to accumulate those issues in this document as the semester progresses.  The idea is that this page can be a go-to document if things aren't working as you expect.  Of course, there are plenty of online resources too.

# List of Issues:

1. [ROS Command Not Found](#1-ros-command-not-found)
2. [ROS Package Not Found](#2-ros-package-not-found)
   * [Not Executable](#not-executable)
3. [Tab Complete Not Working](#3-tab-complete-not-working)
4. [Could not connect to display](#4-could-not-connect-to-display)
5. [Gazebo not running](#5-gazebo-not-running)
6. [Error in REST request](#6-error-in-rest-request)
7. [/usr/bin/env: ‘python\r’: No such file or directory](#7-python-r-run-error)
8. [`catkin_make`: Could NOT find PY_em (missing: PY_EM)](#8-catkin_make-could-not-find-py_em-missing-py_em)
9. [Failed to contact master](#9-failed-to-contact-master)
10. [Gazebo or Turtlebot Launch Error](#10-gazebo-or-turtlebot-launch-error)
___
# 1. ROS Command Not Found

If a ROS command is not found, for example like this:
```
$ catkin_make

Command 'catkin_make' not found, but can be installed with:

sudo apt install catkin
```

Then **do not install** it with the suggested `sudo apt install` command!  Rather, this is most likely caused by your **underlay not being sourced**.  Check to see if your ROS underlay is sourced with `printenv |grep ROS` like this:
```
$ printenv |grep ROS
ROS_VERSION=1
ROS_PYTHON_VERSION=3
ROS_PACKAGE_PATH=/home/dmorris/catkin_ws/src:/opt/ros/noetic/share
ROSLISP_PACKAGE_DIRECTORIES=/home/dmorris/catkin_ws/devel/share/common-lisp
ROS_ETC_DIR=/opt/ros/noetic/etc/ros
ROS_MASTER_URI=http://localhost:11311
ROS_ROOT=/opt/ros/noetic/share/ros
ROS_DISTRO=noetic
```
And you should see plenty of ROS environment variables.  If you do not see these, then source the underlay with:
```
$ source /opt/ros/noetic/setup.bash
```
___
# 2. ROS Package Not Found
If a package you created or downloaded into your workspace `~/catkin_ws` is not running, for example, this:
```bash
$ rosrun lab2_nodes obs_detect.py
[rospack] Error: package 'lab2_nodes' not found
```
Most likely it is because your **overlay has not been sourced**.  Source it with:
```bash
$ source ~/catkin_ws/devel/setup.bash
```
Another reason could be that the python code for your node is not an executable file.  To make it executable:
```bash
$ cd <path_to_package_src>
$ chmod a+x <name_of_python_file.py>
```
If it is a C++ package in your workspace, make sure that you have compiled it:
```bash
$ cd ~/catkin_ws
$ catkin_make
```

## Not Executable
A variation on this error is the following:
```
$ rosrun lab2_nodes obs_detect.py
[rosrun] Couldn't find executable named obs_detect.py below /home/dmorris/catkin_ws/src/student_av/lab2_nodes
[rosrun] Found the following, but they're either not files,
[rosrun] or not executable:
[rosrun]   /home/av/catkin_ws/src/student_av/lab2_nodes/src/obs_detect.py
```
This means the Python file you are trying to run is not executable.  Since this gives you the name of the file it found that is not executable, simply copy it from the above line and paste it into the following line like this:
```bash
$ chmod a+x /home/av/catkin_ws/src/student_av/lab2_nodes/src/obs_detect.py
```
___
# 3. Tab-Complete Not Working

If you are running a native ROS package, tab-complete will fail if the underlay needs to be sourced:  
```bash
$ source /opt/ros/noetic/setup.bash
```
If you are running a package you compiled in your workspace, tab-complete will fail if the overlay needs to be sourced:
```bash
$ source ~/catkin_ws/devel/setup.bash
```
___
# 4. Could not connect to display

You may find various Ubuntu or ROS programs will crash if they attempt to open a graphical window.  For example, this is what I got when running `rqt`:

![Display](.Images/TroubleShootDisplay.png)

There are a few things that could be wrong.  The X-server needs to be running which you can check in Windows from the task-bar icons

![x-launch](.Images/x-launch.png)

If it is not running, start it by typing `xlaunch` in the Windows search bar.

If you are still getting an error, ensure that the `DISPLAY` environment variable is set correctly, as explained in [X_Server](../Setup/Optional/X_Server.md).  You can see look at your `DISPLAY` variable with:
```
$ printenv |grep DISPLAY
DISPLAY=192.168.86.33:0
```
or
```
$ echo $DISPLAY
DISPLAY=192.168.86.33:0
```
Of course you should see the IP address of your host OS.  

If you get this error:
```
Authorization required, but no authorization protocol specified
Error: Can't open display: 192.168.86.33:0
```
Then you should quit and re-start X-Launch, and make sure to disable access control in the Extra Settings window.  

To confirm that your X11 is working, you can do:
```
$ sudo apt install x11-apps
$ xeyes
```
And you should see the xeyes window.

___
# 5. Gazebo not running

In Windows / Mac, if it is not a [Display problem](#4-could-not-connect-to-display), then make sure your `~/.bashrc` file has this line:
```
export LIBGL_ALWAYS_INDIRECT=0   
```
Also type it in any open terminal windows.  Then see if Gazebo runs with the command:
```
$ gazebo
```
Note that in Noetic you should be using Gazebo 11.  You can check with:
```
$ sudo apt-get list --installed | grep gazebo
```
___
# 6. Error in REST request

When running Gazebo in Melodic you may get an error like this:
````
[Err] [REST.cc:205] Error in REST request
libcurl: (51) SSL: no alternative certificate subject name matches target host name 'api.ignitionfuel.org'
````
That does not affect operation, but when running Gazebo it is *always* good to carefully review and address any startup errors.  Making sure there are no errors is likely to save you grief in the long run debugging unexpected results.  You can eliminate this error for future running of Gazebo by editing the file:`~/.ignition/fuel/config.yaml`, and replacing: `api.ignitionfuel.org` with this: `fuel.ignitionrobotics.org`.  Here is a command that will do that for you, and fix this error so it does not show up in subsequent runs:
```bash
$ sed -i 's/api.ignitionfuel.org/fuel.ignitionrobotics.org/g' ~/.ignition/fuel/config.yaml
```

# 7. `python\r` error

You may find you get the following error when running a python node:
```
$ rosrun lab4_color im_capture.py
/usr/bin/env: ‘python\r’: No such file or directory
```
This can happen when a text file has both carriage-returns and line-feeds at the end of each line.  This is the standard in Windows, whereas linux uses only line-feeds.  The solution is to edit the file, in this case `im_capture.py`, and replace the carriage-return line-feed pairs with line-feed characters.  On the bottom right of the VSCode window you'll see the following when the file has carriage-return line-feeds:

![CRLF](.Images/crlf_error.png)

It is quite easy to fix.  Simply click on the `CRLF` text and select `line-feeds` and save the file.  This will replae all the carriage-return-line-feed pairs with line-feeds.  

# 8. `catkin_make`: Could NOT find PY_em (missing: PY_EM)
Catkin_make may need to be told to use Python3, which can be done by using this argument when calling it:
```
$ catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3
```

# 9. Failed to contact master
When running a ROS command you get an error like one of the following:
```
[ERROR] [1610627301.332331100]: [registerPublisher] Failed to contact master at [localhost:11311].  Retrying...
```
```
ERROR: Unable to communicate with master!
```
Then most likely you are not running `roscore` in a separate thread, and you should start it.  

If it is running, but on a different computer of VM, then it may be a problem with the environment variables `ROS_MASTER_URI` and `ROS_HOSTNAME`.  To see how to set these when `roscore` is running on a Turtlebot, have a look in [ROS/Turtlebots_Gazebo.md](../ROS/Turtlebots_Gazebo.md) at section `ROS Network Configuration`.

# 10. Gazebo or Turtlebot Launch Error

One possible error:
```
$ roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
Command 'roslaunch' not found, but can be installed with:

sudo apt install python3-roslaunch
```
Then **do not install python3-roslaunch**.  Rather you need to source the overlay:
```
$ source ~/catkin_ws/devel/setup.bash
```

Another possible error:
````
$ roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
RLException: [turtlebot3_empty_world.launch] is neither a launch file in package [turtlebot3_gazebo] nor is [turtlebot3_gazebo] a launch file name
The traceback for the exception was written to the log file
````
There are two likely causes for this error.  First you may not have installed the Turtlebot packages.  These install both as ROS packages in the `/opt/ros/noetic` folder, and some are built from source in your Catkin workspace.  You can easily check your Catkin workspace with:
```bash
$ ls ~/catkin_ws/src
CMakeLists.txt turtlebot3_simulations
```
There may be more folders here, but you should see `turtlebot3_simulations`. If you do not see that folder, then install the Turtlebot packages as explained in [ROS_Packages.md](ROS_Packages.md).  

If you have the Turtlebot packages, then the most likely reason for the error is that you have not sourced your **overlay**, which is needed for ROS to find those packages in your Catkin workspace.  Simply source your overlay:
```
$ source ~/catkin_ws/devel/setup.bash
```

Another possible error is that Gazebo runs with the below output but just waits without opening a window:
```
$ roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
... logging to /home/dmorris/.ros/log/b372c66c-1adf-11ec-9d14-00155d3d2f63/roslaunch-DM-O-1533.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

xacro: in-order processing became default in ROS Melodic. You can drop the option.
started roslaunch server http://DM-O:39145/

SUMMARY
========

PARAMETERS
 * /gazebo/enable_ros_network: True
 * /robot_description: <?xml version="1....
 * /rosdistro: noetic
 * /rosversion: 1.15.11
 * /use_sim_time: True

NODES
  /
    gazebo (gazebo_ros/gzserver)
    gazebo_gui (gazebo_ros/gzclient)
    spawn_urdf (gazebo_ros/spawn_model)

ROS_MASTER_URI=http://localhost:11311

process[gazebo-1]: started with pid [1549]
process[gazebo_gui-2]: started with pid [1553]
process[spawn_urdf-3]: started with pid [1559]
[ INFO] [1632231265.081496707]: Finished loading Gazebo ROS API Plugin.
[ INFO] [1632231265.082631845]: waitForService: Service [/gazebo/set_physics_properties] has not been advertised, waiting...
[ INFO] [1632231265.143967172]: Finished loading Gazebo ROS API Plugin.
[ INFO] [1632231265.145748382]: waitForService: Service [/gazebo_gui/set_physics_properties] has not been advertised, waiting...
```
Most likely you have not properly started your X-server, see [item 4 above](#4-could-not-connect-to-display), which you should do.

___
### [Back to Index](../Readme.md)






