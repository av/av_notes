#!/usr/bin/env python
''' A state machine with SimpleActionStates instead of standard states.  These have the 
    advantage that you give then a goal which they will fulfill.  This makes
    it very simple to do tasks like waypoint following, which is what this 
    code does.

    Use example:
     roslaunch greenline greenline.launch
     roslaunch greenline mapping.launch   
     rosrun vacbot vac_05_actions_waypoints.py
    or:
     rosrun vacbot vac_05_actions_waypoints.py --start

    The second option has the bot drive to the starting location

    Daniel Morris, April 2020
'''
import rospy
import smach
import math
import argparse
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from std_msgs.msg import Header
from tf.transformations import quaternion_from_euler

def get_start_waypoint():
    wplist = PoseArray(header=Header(frame_id='odom', stamp=rospy.Time(0)))
    pnt1 = Point( -1.5, 1.5, 0.)
    quat1 = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt1,quat1))
    return wplist

def get_line_waypoints():  #A list of pre-defined waypoints
    wplist = PoseArray(header=Header(frame_id='odom', stamp=rospy.Time(0)))
    pnt1 = Point( -0.8, 1.5, 0.)
    quat1 = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt1,quat1))
    pnt2 = Point( 0.5, 1.1, 0.)
    quat2 = Quaternion(*(quaternion_from_euler(0, 0, -90.*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt2,quat2))
    pnt3 = Point( 0., -0.6, 0.)
    quat3 = Quaternion(*(quaternion_from_euler(0, 0, -90*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt3,quat3))    
    return wplist


def init_follower_sm(wplist):
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded','aborted','preempted'])
    # These three outcomes are standard for action states, and are the outcomes of SimpleActionState

    with sm:  # This opens sm container for adding states:
        for i,wp in enumerate(wplist.poses):
            wpose = MoveBaseGoal()
            wpose.target_pose.header.frame_id = "map"
            wpose.target_pose.header.stamp = rospy.Time.now()
            wpose.target_pose.pose = wp
            name = 'wp'+str(i)
            if i < len(wplist.poses)-1:
                nextname = 'wp'+str(i+1)
            else:
                nextname = 'succeeded'
            smach.StateMachine.add( name, 
                                    SimpleActionState( 'move_base', 
                                                       MoveBaseAction,
                                                       goal=wpose,
                                                       server_wait_timeout=rospy.Duration(10.0)),
                                    transitions={'succeeded':nextname,'aborted':'aborted','preempted':'preempted'} )
    return sm
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Waypoint Follower')
    parser.add_argument('--start', action='store_true', default=False, help='Return to start')    
    args, unknown = parser.parse_known_args()  # For roslaunch compatibility
    if unknown: print('Unknown args:',unknown)

    rospy.init_node('Basic_SMACH')
    if args.start:
        wplist = get_start_waypoint()       # Move to starting location
    else:
        wplist = get_line_waypoints()       # Move along path defined by waypoints
    sm = init_follower_sm( wplist )         # Create state machine    
    outcome = sm.execute()
    rospy.loginfo('Final outcome: '+outcome)


