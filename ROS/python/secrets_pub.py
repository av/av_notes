#!/usr/bin/env python
'''
    secrets_pub.py 

    This is a ROS node that demonstrates publishing a random message once per second

    See also: secrets_sub.py

    Daniel Morris, Oct 2020
'''
import rospy
import numpy as np
from std_msgs.msg import String      # Will publish a topic of type String

class PubRandomMessages:

    def __init__(self, message_list, topic_name='secrets', node_name='secret_sender'):        
        self.message_list = message_list                              # Store messages here
        rospy.init_node(node_name)                                    # Initialize ROS node
        self.pub = rospy.Publisher(topic_name, String, queue_size=1)  # Create publisher
        rospy.loginfo('Publishing secret messages to topic: ' + topic_name )
        self.run()                                                    # Run until stopped

    def random_message(self):
        ''' Returns a random message from list '''
        return self.message_list[np.random.randint(len(self.message_list))]

    def run(self):
        rate = rospy.Rate(1)    # ROS set a loop rate of maximum 1 Hz
        while not rospy.is_shutdown():
            self.pub.publish( self.random_message() )  # Publish message
            rate.sleep()        # This sleeps to ensure loop rate is at most 1 Hz
    
if __name__=="__main__":

    secret_messages=['Hello World','The meaning of life','42',"Can't ROS that"]
    PubRandomMessages( secret_messages )

