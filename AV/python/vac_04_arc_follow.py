#!/usr/bin/env python
''' A state machine to pass cmd_vel commands to the robot.  
    This code assumes we know a target location and it follows an arc to this location.  
    This is not how you would actually do navigation or implement pure pursuit, rather 
    it is meant as a very simple example of using a state machine to follow a known path.

    Use example:
     roslaunch greenline greenline.launch
     rosrun vacbot vac_04_arc_follow.py  

    To reset the robot, in Gazebo use

    Daniel Morris, April 2020
'''
import rospy
import smach
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray, Twist
from transform_frames import TransformFrames
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
import numpy as np

def get_target():  # Here target is hard-coded in odom coordinates
    target = PoseArray(header=Header(frame_id='odom', stamp=rospy.Time(0)) )
    target.poses.append(Pose(Point( 0., -0.5, 0.),Quaternion(0.,0.,0.,1.)))    
    return target

class IsStuck():
    ''' Tests if robot is stuck '''
    def __init__(self):
        self.nstuck = 0
    def test(self, cmd_speed):
        try:
            msg = rospy.wait_for_message("odom", Odometry, 0.2)
            if np.abs(msg.twist.twist.linear.x) < np.abs(cmd_speed) / 100:
                self.nstuck += 1
            else:
                self.nstuck = 0
        except rospy.ROSException:
            pass
        if self.nstuck >= 10:
            return True
        else:
            return False

class ArcDrive(smach.State): 
    def __init__(self):
        smach.State.__init__(self, outcomes=['keepgoing','reached_goal','failed','aborted'],
                                   input_keys=['in_target'])
        self.trans = TransformFrames()
        self.move_pub = rospy.Publisher('cmd_vel', Twist, latch=True, queue_size=1)  
        self.stuck = IsStuck()

    def execute(self, userdata):
        #Find target location relative to current location in base_footprint
        target = self.trans.pose_transform(userdata.in_target, 'base_footprint' )
        targetPoint = target.poses[0].position
        vel = Twist()
        keepgoing = targetPoint.x > 0 #Check if target is in front of robot
        if keepgoing:  # Only command a non-zero velocity if target is in front of robot
            vel.linear.x = 0.1  #Use fixed linear velocity
            k = 2.*targetPoint.y / (targetPoint.x**2 + targetPoint.y**2)  #Eq. Pure Pursuit
            if abs(k) > 5.:  # Prevent turning radius < 0.2m for smoother trajectories
                k = 5. * k/abs(k)
            vel.angular.z = k * vel.linear.x  # omega = k * v
        self.move_pub.publish(vel)
        rospy.sleep(0.2)        
        if rospy.is_shutdown():
            return 'aborted'            
        elif self.stuck.test(vel.linear.x):
            rospy.loginfo('Robot is stuck')
            return 'failed'
        elif keepgoing:
            return 'keepgoing'
        elif (targetPoint.x**2 + targetPoint.y**2) < 0.1**2:
            return 'reached_goal'
        else:
            rospy.loginfo('Unable to plan path to target')
            return 'failed'

def init_arcdrive_sm():
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['reached_goal','aborted','failed'])
    sm.userdata.sm_target = get_target()  #Target in odom coords
    sm.set_initial_state(['DRIVE'])
    
    with sm:  # This opens sm container for adding states:
        smach.StateMachine.add('DRIVE', ArcDrive(), # Add state and mapping for IO hooks
                transitions={'keepgoing':'DRIVE',
                             'reached_goal':'reached_goal',
                             'aborted':'aborted',
                             'failed':'failed'},
                remapping={'in_target':'sm_target'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('Arc_Driving_SMACH')
    sm = init_arcdrive_sm( ) # Create state machine    
    outcome = sm.execute()   # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)


