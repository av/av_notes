# Remotely Editing with VSCode

### Author: Daniel Morris 
### [Index](Readme.md)
## Contents

* [Introduction](#introduction)
* [Remote Editing in WSL](#remote-editing-in-wsl)
* [Remote Editing via SSH](#remote-editing-via-ssh)
  * [Direct SSH](#direct-ssh)
  * [Proxy SSH to HPCC](#proxy-ssh-to-hpcc)
* [Remote Editing in a Docker Container](#remote-editing-in-a-docker-container)

# Introduction

VSCode has a powerful remote editing feature.  This enables you to have code on a remote Ubuntu computer, and edit it directly on your local computer.  As part of this you can run debug in your local VSCode which will run on the remote computer.  This document explains how to setup remote editing on WSL, SSH and Docker.  

___
# Remote Editing in WSL

Instructions for installing and using with WSL are here: [https://code.visualstudio.com/docs/remote/wsl](https://code.visualstudio.com/docs/remote/wsl).  Install the `Remote - WSL` extension in your Windows VSCode from the Marketplace icon on the left bar.  Then in your WSL Ubuntu shell type:
````
code
````
This should open a VSCode instance in Windows with a `WSL:Ubuntu-20.04` label in the bottom left. 

___
# Remote Editing via SSH


## Direct SSH

Use this procedure if you are connecting to your Ubuntu instance via SSH (for example to Ubuntu on other computer).  Instructions for installing VSCode and the Remote Extension on your local machine are here: [https://code.visualstudio.com/docs/remote/ssh](https://code.visualstudio.com/docs/remote/ssh).   

Once installed, run VSCode on your main OS (Windows / Mac).  Click on the bottom left corner icon, and select: `Remote-SSH: Open Configuration File...` 
<p align="middle">
<img src=".Images/remotessh.png" width="350">
<img src=".Images/OpenConfigFile.png" width="350">
</p>

On Windows, choose the config file in your user folder as shown.  This opens your config file for remote connections.  Create a new 3-line entry like that shown here.  Choose a name for your Host (in this case `vbox`), provide its IP address and your user name.

![Open the User config file](.Images/selectSSH.png) 

An entry in this config file would look like this:
```
Host compute01
    HostName compute01.egr.msu.edu
    User dmorris
```
Use your own NetID in the `User` line.  Save the config file, and next time you select the bottom left icon, choose: `Remote-SSH: Connect to Host...`.  Then select the host name you just created.  You'll need to indicate that you are connecting to a Linux computer, and log in when prompted.

## Proxy SSH to HPCC

Some remote computer systems use a proxy server to access them.  This includes the HPCC at MSU.  Details for connecting to the HPCC are [here](Docker_HPCC.md).  The recommended way to edit files on HPCC is using VSCode via SSH.  VSCode SSH configuration to HPCC via a proxy is done as follows.  As above, edit the `.ssh\config` file and add a gateway entry and a development node entry like this
```
Host gateway
    HostName hpcc.msu.edu
    User dmorris

Host dev-intel14
    HostName dev-intel14
    User dmorris
    ProxyCommand C:\Windows\System32\OpenSSH\ssh.exe -W %h:%p gateway   
```
Of course you will use your own user NetID in the `User` lines.  This connects to the development node: `dev-intel14`, and additional development node entries can be added if you wish to use them.  When connecting you will need to input your password twice (once for the gateway and once for the node).  You can avoid this by setting up a public/private encryption key, or minimize password entry by creating a workspace within VSCode.

___
# Remote Editing in a Docker Container

Information on editing documents in a container is here: [https://code.visualstudio.com/docs/remote/containers](https://code.visualstudio.com/docs/remote/containers). 
Start by installing the Docker extension in VSCode: ![Install Docker Extension](.Images/Docker.png)

Make sure your docker container is running with the `docker run ...` command.  Then click on the Docker icon on the left bar and you should see your docker container running with a green triangle in front of it.  (A red square means it is stopped and needs to be started.)  Right click on this and select `Attach Visual Studio Code`:  
![Attach VSCode to running Docker Container](.Images/DockerAttach.png)

This should open a new VSCode that is remotely editing your Docker container.  

___
### [Back to Index](Readme.md)
