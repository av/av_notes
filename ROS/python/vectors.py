#!/usr/bin/env python
'''
    vectors.py 

    Some examples of vector manipulation with Numpy

    Daniel Morris, Oct 2021
'''
import numpy as np

if __name__== '__main__':

    # Let 3 points define a plane
    p0 = np.array([3.,2.,1.])
    p1 = np.array([-1.,4.,0.])
    p2 = np.array([2.,-3.,3.])

    # 2 vectors in the plane:
    v0 = p1 - p0
    v1 = p2 - p0

    # Let's find the vector orthogonal to the plane:
    plane_perp = np.cross(v0,v1)

    # Let's make it a unit vector:
    plane_perp /= np.linalg.norm( plane_perp )
    print(f'Unit normal to plane: {plane_perp}')

    # Let's define a coordinate system with 
    # x axis along v0, and
    # z axis along plane_perp
    # First let's find y axis as y = cross(z, x)
    x = v0 / np.linalg.norm(v0)     # must be unit vector
    z = plane_perp                  # already a unit vector
    y = np.cross(z, x)              # Since x and z are orthogonal and unit, y will be unit

    # Confirm that y is unit normal:
    print(f'y norm value: {np.linalg.norm(y)}')

    # Confirm that x is orthogonal to y:
    print(f'Inner product of x and y: {np.dot(x,y)}')

    # Rotation defining this coordinate system relative to world:
    # Note: it is very important that these all be unit vectors
    R = np.column_stack( (x,y,z) )


    print('Unit vectors:')
    print('x:',x)
    print('y:',y)
    print('z:',z)

    print('Rotation matrix:')
    print(R)
