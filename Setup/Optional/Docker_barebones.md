# Docker On Home PC

### Author: Daniel Morris
### [Index](Readme.md)
## Contents
* [Docker Downloads](#docker-downloads)
* [ROS Docker Images](#ros-docker-images)
* [Test Docker-ROS](#test-docker-ros)
___

Docker is purely optional for this course.  It provides an easy way to set up a full Ubuntu and ROS environment, and to have multiple versions of these running simulaneously.  But Docker is not necessary if you have Ubuntu as part of WSL or other.  It also can be pretty memory intensive, so not ideal if you are memory constrained.

# Docker Downloads

To install Docker, follow the installation instructions below depending on your environment:
* [Mac installation](https://docs.docker.com/docker-for-mac/install/)
* [Windows computers with WSL 2](https://docs.docker.com/docker-for-windows/wsl/) (this is the preferred option for Windows)
* [Windows computers](https://docs.docker.com/docker-for-windows/install/)  without WSL 2

# Docker Images and Containers

There is plenty of online documentation on Dockers images and containers.  In brief, the process of creating a Docker container is as follows.  First a Docker file is created (such as the one I built [here](docker/noetic_full.Dockerfile)) which specifies all the operating system and application software components needed.  From this Dockerfile, a read-only image is built.  The resulting Docker image can be large as it includes all the code and executables.  To make a change to an image requires rebuilding it.  Now a container is a running instance of an image.  It includes additional read/write memory that can be changed.  Containers are compute environments isolated from your system, and can be configured to pass data to and from your system if needed.  Containers are usually meant to be temporary; you delete them when you're done with them, although it is possible to keep a container around for a long time.  A container acts like a complete virtual machine (although uses fewer resources), and can support multiple shells and multiple applications, just like a virtual machine.  

# ROS Docker Images

There are are many pre-built Docker images available for anyone to use.  The official ROS images hosted by Docker are here: [https://hub.docker.com/_/ros](https://hub.docker.com/_/ros), and in addition there are desktop ROS images with additional packages maintained by OSRF here: [https://hub.docker.com/r/osrf/ros/tags](https://hub.docker.com/r/osrf/ros/tags).  It is simple to pull any image you like and create a container from it, as illustrated next.

# Test Docker-ROS

There is a very basic ROS-Docker tutorial here: [wiki.ros.org/docker/Tutorials/Docker](http://wiki.ros.org/docker/Tutorials/Docker).  From your command line try:
````
docker pull ros:noetic-ros-base
````
With that one command you have got yourself a ROS install!  Now to run it to create a container with this command:
````
docker run -it ros:noetic-ros-base
````
The `-it` option is for an interactive terminal.  We are now in a container running Ubuntu 20.04 with the base ROS package installed.  

![ROS container](.Images/BaseROS.png)

Check if the underlay has been sourced:
````
printenv |grep ROS
````
![Check Underlay](.Images/DockerROS_Underlay.png)

If those environment variables are not set, then you need to source the underlay with this:
````
. /opt/ros/noetic/setup.bash
````

Now try running a ROS command:
````
roscore
````
![Run roscore](.Images/DockerROS_roscore.png)

Ah, it looks good.  Stop `roscore` with `Ctrl-c`.  To stop your container, simply exit the shell with:
````
exit
````
Have a look at your latest Docker container, namely the one you just stopped:
````
docker ps -l
````
![Latest Docker](.Images/DockerPS.png)

To see all your Docker containers use the `-a` option instead of the `-l` option.  Here you can see my Docker was randomly named `sweet_dhawan`.  To remove this container simply execute `docker rm` with this name:
````
docker rm sweet_dhawan
````
___
### [Back to Index](Readme.md)
