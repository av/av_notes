#!/usr/bin/env python
'''
    secrets_sub_simple.py 

    This is a ROS node that subscribes to a topic and displays the message in an OpenCV window
    Key things to notice on this code:
       -- Subscribes to a String topic
       -- Uses OpenCV for plotting all within a single thread (so no need for locks)
            Does this by only calling the ImageRotator class within the subscriber callback
            This includes the initialization of ImageRotator
       -- Compare to secrets_sub.py which uses locks to enable independent rotating and callbacks

    See also: secrets_pub.py, av_notes/Setup/python/hello_user.py, secrets_sub.py

    Daniel Morris, Oct 2020
'''
import rospy
from std_msgs.msg import String
from hello_user import ImageRotator
from threading import Lock

class MessageDisplay:

    def __init__(self, topic_name='secrets', node_name='secret_finder'):
        self.imr = None                             # Allocate space for ImageRotator, but don't initialize until in subscriber
        rospy.init_node(node_name,anonymous=True)   # Initialize ROS node
        rospy.Subscriber(topic_name, String, self.read_callback)  # Subscribe to topic of strings
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        ''' Call-backs with the subscribed message use a separate thread from the main thread
        '''
        if self.imr is None:
            self.imr = ImageRotator()      # Initialize in callback since this class handles OpenCV plotting functions

        self.imr.create_hello_image( msg.data )
        self.imr.show_next_image()


if __name__=="__main__":

    md = MessageDisplay()       # Initialize the node and subscriber callback
    rospy.spin()                # Wait for callbacks


