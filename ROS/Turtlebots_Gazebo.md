# Turtlebots: In Gazebo and Real

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [Turtlebots as Mobile Platforms](#turtlebots-as-mobile-platforms)
* [Turtlebots in Gazebo](#turtlebots-in-gazebo)
  * [Install Turtlebot Software](#install-turtlebot-software)
  * [Setup Environment Variables](#setup-environment-variables)
  * [Run a Simulated Turtlebot](#run-a-simulated-turtlebot)
  * [Teleoperation of the Turtlebot](#teleoperation-of-the-turtlebot)
  * [Commanding the Turtlebot](#commanding-the-turtlebot)
  * [Odometry Topic](#odometry-topic)
* [Example Node to Drive in a Circle](#example-node-to-drive-in-a-circle)
* [Real Turtlebots](#real-turtlebots)
  * [Arena Turtlebots](#arena-turtlebots)
  * [Hands-on Turtlebots](#hands-on-turtlebots)
    * [Caring for the Turtlebot](#caring-for-the-turtlebot)
    * [WiFi Network](#wifi-network)
    * [Network Configuration](#network-configuration)
  * [Running a Real Turtlebot](#running-a-real-turtlebot)


# Turtlebots as Mobile Platforms

There is a good introduction and full documentation on Turtlebots here: [https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview](https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview)

This document will run both virtual Turtlebots in a virtual 3D environment as well as real Turtlebots.  Gazebo provides a simulation of both the environment and the Turtlebot.  The commands you use to control the Turtlebot in Gazebo are exactly the same as the commands you use to control a physical Turtlebot.  The code you write does not need to know whether it is controlling a real or virtual Turtlebot.  The difference will be determined by whether the Turtlebot ROS nodes on a real Turtlebot are connected to `roscore`, or if the Gazebo Turtlebot ROS nodes are connected to `roscore`.

![Turtlebot](.Images/turtlebot3.png)

# Turtlebots in Gazebo

## Install Turtlebot Software

While ROS has been installed on your ROS VM, there are additional packages needed by the class, so install them as described in [Setup/ROS_Packages](../Setup/ROS_Packages.md).  This will install some of the Turtlebot software from source in your `catkin_ws` workspace, so it is necessary to source the **overlay** in each shell you run the Turtlebot:
```
source ~/catkin_ws/devel/setup.bash
```

___
## Run a Simulated Turtlebot

To launch an empty world Gazebo with a single Turtlebot, use the commands:
````
roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
````
Note: using tab-complete with the node name above you can find alternate Turtlebot world models such as `turtlebot3_world.launch` that you could place the Turtlebot in.

The above command adds a Turtlebot to Gazebo, but does not start up the turtlebot machinery, including its state publisher and 3D description publisher.  To start these, keep your Gazebo running and in a new shell (after sourcing the overlay) run:
```bash
roslaunch turtlebot3_gazebo turtlebot3_gazebo_rviz.launch
``` 
In addition to starting the state publisher, this will bring up an RViz instance.  This is also a convenient way to start rviz configured to view the Turtlebot and is laser scan.

The first command will show your Turtlebot3 Burger in Gazebo like this: 

![Turtlebot in Gazebo](.Images/gazeboTurtlebot.png)

The `turtlebot3_empty_world.launch` launch file puts the Turtlebot in an empty Gazebo world.  You can put it in other pre-defined worlds by using other launch files in this folder: `~/catkin_ws/src/turtlebot3_simulations/turtlebot3_gazebo/launch`.  

## Teleoperation of the Turtlebot

You can teleoperate your turtlebot with the following command:
```
roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```
You will need to source just your *underlay* as the working `turtlebot3_teleop` package is installed in `/opt/ros/noetic`.

## Commanding the Turtlebot

In a new shell, verify you can see the topics being published by Gazebo using `rostopic list` from the shell.  Here is what I see:
```bash
$ rostopic list
/clock
/cmd_vel
/gazebo/link_states
/gazebo/model_states
/gazebo/parameter_descriptions
/gazebo/parameter_updates
/gazebo/set_link_state
/gazebo/set_model_state
/imu
/joint_states
/odom
/rosout
/rosout_agg
/scan
/tf
```
If you get an error `rostopic command not found`, make sure to source your underlay.

Let's take a look at the `/cmd_vel` topic.  This is a topic for commanding velocities to the Turtlebot.  Use the command `rostopic info <topic_name>` as follows:
```bash
$ rostopic info /cmd_vel
Type: geometry_msgs/Twist

Publishers: None

Subscribers:
 * /gazebo (http://docker-desktop:46763/)
```
We see that this ROS topic has a message type of `geometry_msgs/Twist`, and that `/gazebo` subscribes to it.  Right now there are no nodes publishing to it.  Let's command the Turtlebot to do something by publishing a message to this topic.  Try the following:
```bash
$ rostopic pub /cmd_vel geometry_msgs/Twist "linear:
  x: 0.2
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.1"
```
Note: use tab-complete to quickly do this.  Start by typing `rostopic pub /cmd_vel <tab><tab>`.  ROS knows the message type of the `/cmd_vel` topic, so it will auto-complete it with all-zero values.  You can use the left arrow key to edit the numbers and press Enter when done.  

But before you press Enter, stop and make a prediction as to what the this will cause the Turtlebot to do.  What shape will the Turtlebot move in?  Now see if you got it right.

This publisher latches the command.  That means the message stays in the ROS bus until superceded by another command.  To stop the robot, repeat the above command, but publish an all-zero velocity.

## Odometry Topic

The `/odom` topic is published by the Turtlebot and contains its estimated pose and motion.  Try exploring this topic and its components using the `rostopic info` command.


___
# Example Node to Drive in a Circle

We have seen how to publish a velocity command in the terminal.  Now let's create a node that drives the robot in a circular arc.  Here is the code I wrote to do that:

```python
import argparse
import rospy
from geometry_msgs.msg import Twist

def CircleDrive:
    def __init__(self):
        rospy.init_node('move')
        self.pub = rospy.Publisher('/cmd_vel', Twist, latch=True, queue_size=1)
            # latch: keeps published topic current until next published topic
	
    def start(self, linear, angular ): 
        move = Twist()                 # Initialized all-zero values
        move.linear.x = linear         # Linear velocity in the x axis
        move.angular.z = angular       # Angular velocity in the z axis
        self.pub.publish(move)  

    def stop(self): 
        move = Twist()                 # Initialized all-zero values
        self.pub.publish(move)  

    def drive(self, time=2.0, linear=2.0, angular=1.5):
        info = f'Driving for {time} sec with linear speed {linear} m/s and angular speed {angular} rad/s'
        rospy.loginfo(info)
        self.start( linear, angular )
        rospy.sleep( time )
        self.stop()
        rospy.sleep( 0.1 )     # Ensure publishing completes before quitting
        rospy.loginfo('Done driving')

if __name__== '__main__':
    parser = argparse.ArgumentParser(description='Circular Arc Arguments')
    parser.add_argument('--time',    default=5.0, type=float,    help='Time to drive')
    parser.add_argument('--linear',  default=0.2, type=float,    help='Linear speed')
    parser.add_argument('--angular', default=0.8, type=float,    help='Angular speed')
    args, unknown = parser.parse_known_args()  # For roslaunch compatibility
    if unknown: print('Unknown args:',unknown)

    av = CircleDrive()
    av.drive(args.time, args.linear, args.angular)
```

Notice that the python code accepts three optional arguments specifying how long the Turtlebot should drive for, and what its linear and angular speeds should be.  Within the code, we publish a `Twist` message to the `/cmd_vel` topic.  The first publish sets the target linear and angular speed, and the second publish sends and all-zero `Twist` message which stops the robot.  The publisher is initialized with `latch=True` and tells the robot to maintain the velocity until the next message is published to the topic.

Now let's try this out.  First, bring up a Turtlebot, either a simulated one or a real one.  Then, if you are using a Docker container for Gazebo on WSL, you'll need to start another Docker container to run Noetic.  When you do this, don't forget to source the overlay, and `source ~/env/work/bin/activate` to get your `python` environment up.

Then let's create a package for this node and run it as follows:
```
$ cd ~/catkin_ws/src
$ catkin_create_pkg circle rospy
$ cd circle/src
$ cp <path_to_my_repo>/av_notes/ROS/python/circle_drive.py .
$ chmod +x circle_drive.py
```
With Docker, the `<path_to_my_repo>` could either to be a repo you have cloned inside the container, or else through a mounted folder to a folder in your host OS.

Now we are ready to run.  You can run it with default values using:
```
$ rosrun circle circle_drive.py
```
Or you can run with command-line arguments like this:
```
$ rosrun circle circle_drive.py --time 10 --linear 0.3 --angular 0.6
```
Additionally, there are instructions in [Setup/VSCode.md](../Setup/VSCode.md) for how to run this code in the VSCode debugger.  It is a good idea to try this, as doing this will be useful for future ROS nodes you create.


# Real Turtlebots

The remaining portion of this document is not necessary for Spring Semester 2021 as we are operating remotely.  Instead we will be working on real-world projects for AutoDrive.  

## Arena Turtlebots

In a future version of this course we may control an on-campus Turtlebot in an arena from off-campus.  Details TBD.

## Hands-on Turtlebots

If you get to operate a turtlebot hands-on, then the following are useful notes for caring for them and connecting them.

### Caring for your Turtlebot

The Turtlebots are battery powered, and operate for between 30 and 60 minutes on a full charge.  When turned off, they will gradually drain the battery, and will destroy the battery if left to completely drain.  So it is essential that if you are not going to use the Turtlebot for more than a few days that you disconnect the battery.  That is done by pulling appart the connector shown below:

![Disconnect the battery](.Images/turtlebotBattery.png)

Turtlebots can move suddenly and unpredictably.  A fall from a table is likely to damage or destroy the Lidar and other parts.  Thus, even though it is more convenient to observe them on a tabletop, **always run them on the ground**, not on a tabletop.  

### WiFi Network

At MSU the Turtlebots will be configured to use the *turtlebot* engineering network as this unblocks the ports needed by ROS which are blocked by the standard engineering network.  

At home, network configuration is simple.  Attached a keyboard and mouse via USB, and a monitor via HDMI, to the Raspberry Pi board on the Turtlebot.  Turn on the Turtlebot and the Raspiberry Pi OS will come up with a GUI.  From this you can select your home network and enter the password.  

You will need the IP address of your Turtlebot.  If you have a monitor and keyboard already connected, then open a terminal window and use `hostname -I` to find the IP address and note it down.  Alternatively, some of the Turtlebots have a LCD screen that shows the IP address, as shown below:

![Turtlebot LCD](.Images/turtlebotLCD.png)

Once you have the IP address, you can connect to the Turtlebot from another computer on the network with `ssh`:
```bash
$ ssh pi@<turtlebot_ip_address>
```
Enter the password you have been provided with in the D2L course notes.

### ROS Network Configuration

When multiple computers communicate over ROS, then a couple additional steps need to be taken to connect them.  It is the job of the ROS master to connect every pair of nodes that publish or subscribe to the same topic.  The actual data does not flow through the ROS master, if just makes the introduction and sets the ports through which the nodes communicate.  The consequence is that all nodes need to know the IP address of ROS master, and they all need to tell the ROS master what their IP address is.

First choose a computer on which the ROS master will run.  If you have one Turtlebot, then running `roscore` on the Turtlebot is simplest.  To tell ROS where roscore is running you need to set the environment variable `ROS_MASTER_URI` with this IP address on all computers running nodes (including that running `roscore`).  Simply type:
```bash
export ROS_MASTER_URI=http://<turtlebot_ip_address>:11311
```
The `:11311` is the default port used by the ROS master.  Additionally, since each node needs to tell the ROS master its IP address, set the `ROS_HOSTNAME` environment variable:
```bash
export ROS_HOSTNAME=<local_ip_address>
```
Here `<local_ip_address>` is the IP address of the computer running the node.  

**Important** When setting environment variables like those above, make sure there are **no** spaces around the `=` sign.  

An illustration of Turtlebot connectivity is shown here:

![Turtlebot network IP addresses](.Images/TurtlebotConnectivity.png)

On the Turtlebot, both of these environment variables are set in the `.bashrc` file.  Have a look at it by typing `cat ~/.bashrc`.  The relevant lines in my file are:
```bash
ip_turtle=`hostname -I | cut -f 1 -d ' '`
export ROS_MASTER_URI=http://${ip_turtle}:11311
export ROS_HOSTNAME=${ip_turtle}
```
This means you do not need to manually set these variables on the Turtlebot, but you will need to set them on your local computer.

## Running a Real Turtlebot

The Turtlebot software has already been installed on the Turtlebot.  To run it, have a look at your `.bashrc` file by typing: `cat ~/.bashrc`.  The relevant lines are:
```bash
alias start_robot='roslaunch turtlebot3_bringup turtlebot3_robot.launch'
alias start_lidar='roslaunch turtlebot3_bringup turtlebot3_lidar.launch'
alias start_camera='roslaunch turtlebot3_bringup turtlebot3_rpicamera.launch'
```
You can see three shortcut commands for starting up your robot.  The one you will need for now is:
```bash
$ start_robot
```
This calls a launch file which will take care of starting `roscore` (so there is no need for you to do this yourself), as well as the other Turtlebot nodes.  When you call this it will run continuously in the current terminal.  To run another command on the Turtlebot you will need to ssh to it again from a separate window.  Connecting again, I can see the following topics:
```bash
pi@turtlebot-fac:~ $ rostopic list
/battery_state
/cmd_vel
/cmd_vel_rc100
/diagnostics
/firmware_version
/imu
/joint_states
/magnetic_field
/motor_power
/odom
/reset
/rosout
/rosout_agg
/rpms
/scan
/sensor_state
/sound
/tf
/version_info
```
You will notice the `/cmd_vel` topic, which we saw on the virtual Turtlebot in Gazebo.  Try sending a command to it as you did in [Commanding your Turtlebot](#Commanding-your-Turtlebot).

**Important** Whenever commanding a Turtlebot, ensure it is on the floor and not on a table-top.  They can be tricky to stop and falling off the edge could easily destroy the Lidar and other components.


___
### [Back to Index](../Readme.md)
