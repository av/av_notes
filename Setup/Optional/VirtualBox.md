# Setup Guide for VirtualBox

### Author: Daniel Morris
### [Index](Readme.md)
___
These instructions are optional.  Setup a VirtualBox only if you would rather not go the **WSL** or **Docker route**.  Note that as explained in [Ubuntu docs](Ubuntu.md), this is **not** a recommended option for this course.

VirtualBox is a free and relatively simple way to run one OS within another in a Virtual Machine (VM).  Being free, it has some limitations especially no ability to leverage the GPU from the host computer.  It also runs the virtual OS relatively slowly compared to a direct intall, and uses a fair amount of resources.  Thus it is not a top choice for this class.  However, it does give a full Ubuntu instance for the course and so is a relatively simple way to set up a complete environment for the course.  Another key advantange over WSL is that it connects to the same network as your host computer, and so can be used to communicate with the TurtleBots via WiFi (unlike WSL).

An alternative free option for Windows in VMWare's Player, and a not-free optoin for the Mac is VMWare's Fusion.  These are reported to be more efficient than VirtualBox.  You are welcome to explore these alternatives on your own.

The following are instructions for installing VirtualBox and creating and Unbuntu instance:

1. Download and install VirtualBox from: [https://www.virtualbox.org/](https://www.virtualbox.org/)
2. Download an Ubuntu .iso file for your OS, most likely: `ubuntu-20.04.X-desktop-amd64.iso` where `X` is the most recent release.  This can be found at: [https://releases.ubuntu.com/20.04/](https://releases.ubuntu.com/20.04/)
3. Run VirtualBox and create a new VM instance.  I called min `Ubuntu 20`.   ![New VM](.Images/VBoxNew.png) Before running it, set the following preferences:
    - Under 'Settings', 'Network', set your Wi-Fi adapter to Bridge mode if not already set this way. Disable NAT if is selected.
    - If your computer as muliple cores, set the CPU count to 2
    - You may wish to enable copying between the VM and the host under 'Settings', 'General', 'Advanced'
    - Set the RAM to at least 2048 MB, and preferably 4096 MB if your laptop has at least 6000 MB RAM available.
    - Under 'Settings', 'Storage', 'Controller IDE' add your `.iso` file. ![Load iso file](.Images/VBaddIso.png)
    - You can start your new Ubuntu instance.
4. Debugging Virtual Box 
    * If your Ubuntu instance will not run, make sure that
        - Virutalization is enabled in your bios
        - You may need to disable [Hyper-V](https://docs.microsoft.com/en-us/troubleshoot/windows-client/application-management/virtualization-apps-not-work-with-hyper-v)
    * Error on Mac: ‘The Installation Failed’,  [try this](https://medium.com/@DMeechan/fixing-the-installation-failed-virtualbox-error-on-mac-high-sierra-7c421362b5b5)
    * The Ubuntu screen size is very small. To resize: from running Ubuntu top menu select `View/Auto Resize Guest Display` as in: ![Resize Screen](.Images/AutoResize.png)
    If this option is not available, install VBox Guest Additions from the Devices menu: ![Insert Guest Additions](.Images/GuestAdditions.png)
    Make sure to run it when prompted.  Then after rebooting you should be able to resize as described above.  


You should now have a running instance of Ubuntu 20.04.  This section describes a few Ubuntu usage and setup preliminaries for your Ubuntu instance.

* To start a terminal, click on the left-lower 9-dot icon, and start typeing `terminal`.  You'll see a terminal icon appear and click on this.   You'll find that adding tabs to the terminal with `crtl-shift-t` (or by clicking the top-left corner of the terminal) will be very useful for ROS. 
<p align="middle">
<img src=".Images/Command.png" width="250">
<img src=".Images/CommandTerminal.png" width="250">
<img src=".Images/Terminal.png" width="250">
</p>

* In the terminal, type the following commands to install needed debian packages:
````
   sudo apt update
   sudo apt install net-tools openssh-server curl git python3-pip
````
* ROS 1 used to use Python 2, but Noetic changes this to Python 3.  You should set Python 3 to be the default using:
````
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
````
* In my VM I prefer to turn off automatic screen locking (as my main OS does this anyway). To do this, pull up the Settings window by typing "Settings" into the 9-dot command.  Then select `Privacy` option from the left-hand side menus.  From this select `Screen Lock` and turn off `Automatic Screen Lock` as shown below: ![Screen Lock Off](.Images/ScreenLockOff.png)

___
### [Back to Index](Readme.md)
