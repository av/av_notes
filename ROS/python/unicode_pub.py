#!/usr/bin/env python
'''
    unicode_pub.py 

    This is a ROS node that demonstrates publishing Int32 values to a topic
    The class inputs a character, converts it to unicode and publishes it as a topic
    This is not a practical string publisher (one would use the string type for that)

    See also: unicode_sub.py

    Daniel Morris, Oct 2020
'''
import rospy
from std_msgs.msg import Int32

class UniEncoder:

    def __init__(self, topic_name='uni_char', node_name='uni_pub'):
        rospy.init_node(node_name) # Initialize ROS node
        # Create a publisher that publishes to topic 'uni_char'
        self.pub = rospy.Publisher(topic_name, Int32, queue_size=10)
        rospy.loginfo('Publishing to topic: ' + topic_name )
        
    def pub_string(self, pstring):
        ''' Input a string and publish each character in unicode '''
        for c in pstring:
            self.pub.publish(ord(c))  # Publish unicode for each character in string
        self.pub.publish(ord('\n'))   # Publish a newline to indicate end-of-string
        
    def run(self):
        ''' Input and publish until interrupt '''
        while not rospy.is_shutdown():  # Exit loop if Ctrl-C pressed
            pstring = input('Publish: ')
            self.pub_string(pstring)
    
if __name__=="__main__":

    myencoder = UniEncoder()
    myencoder.run()  

