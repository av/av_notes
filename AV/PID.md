# PID (Proporional, Integral, Derivative) Line Follower

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [PID Controller](#pid-controller)
  * [PID Code Example](#pid-code-example)
  * [Proportional Gain](#proportional-gain)
  * [Integral Gain](#integral-gain)
  * [Derivative Gain](#derivative-gain)
* [Line Follower](#line-follower)
___
# PID Controller

The goal in this module is to introduce feedback-control and give you intution into using feedback control for a robot or AV.  The basic concepts are powerful and useful even for non-experts, although if you are going to program a real AV controller, you will want to take a control course or two, as one lecture will not tell you all you need.

Recall the open-loop behavior of your robot as it tried to follow a square trajectory.  If it departed from the planned path, it had no way of self-correcting.  As a result it was important to very precisely initialize the robot and set its commanded velocities.  Even so, the results were still fairly brittle, and if something were to perturb your robot, it could easily diverge from its target trajectory.

Now a feedback loop introduces the opportunity for the robot to correct its path if it diverges from the target path.  For example, in a proportional controller, when there is a large divergence, the control signal will be large and will push the vehicle more strongly to reduce the divergence.  You may not even need a prior target path.  In the case of line following, a control loop will seek to keep the robot following a line based purely on correcting its path if it diverges from the line.  It can thus be both simpler and more robust that prior-planned trajectories.

Lets define the signed error *e(t)* to be:

*e(t) = r(t) - y(t)*

These are all functions of time *t*.  The target state *r(t)* is where we want our state to be at any given time.  While we cannot necessarily know the state, we can measure its value: *y(t)*.  By driving *e(t)* to zero, we make *y(t)* approach *r(t)*.  The actual control signal, *u(t)*  depends on *e(t)* in 3 different ways: proportional to *e(t)*, proportional to the time integral of *e(t)*, and proportional to the time derivative of *e(t)*.   The following sections will give intuition into each of these.  

![PID](.Images/PID.png)

## PID Code Example

To help you build your own intuition, a simple PID controller for the Turtlebot is provided.  It controls the yaw heading of the Turtlebot, so the *Target(t)* is a target yaw value, and *State(t)* is the actual or measured yaw value.  You can adjust the parameters to gain a feeling for how adjusting each parameter affects the behavior of the controller.  The main function is [AV / python / cmd_yaw.py](python/cmd_yaw.py) which imports a PID class from [AV / python / pid.py](python/pid.py).  To use it to control the yaw heading of the Turtlebot, first start-up Gazebo, either an empty world or the Greenline world, such as as follows:
```
$ roslaunch greenline greenline.launch
```
Now run `cmd_yaw.py` as follows:
```
$ python cmd_yaw.py <yaw_val> <Kp> <Ki> <Kd> --use_rate
```
This will command the Turtlebot's yaw rate so that its yaw follows two step functions.  The height of the step (namely the amount it turns) is `yaw_val` in radians, and a good value is `1`, i.e. about 60 degrees.  Then you can select any values you wish for `Kp` proportionality gain, `Ki` integral gain, and `Kd` derivative gain.  The `--use_rate` is an optional argument, which if provided will pass in the rate of change in yaw from the IMU to the PID function.  When provided, this used as part of the time derivative of *Error(t)* rather than calculating it numerically. It only affects the derivative term, and can improve responsivity, as shown below.

## Proportional Gain

Proportional control, i.e. `Kp>0`, will input a control signal that is proportional to the *Error(t)*, and that will tend to reduce the *Error(t)*.  The larger *Error(t)*, the larger the control signal, and the faster the state will change.  Here is is an example of proportional control of the Turtlebot, with the gain values specified in the title:

![Kp=1](.Images/pid100.png)

Notice that as the yaw approaches the target yaw its rate slows, which is what one would expect with proportional gain.  A typical control goal is to reach the target as quickly as possible.  If we increase the proportional gain from 1 to 4, we get:

![Kp=4](.Images/pid400.png)

This approaches the target much faster, and then overshoots.  At a certain level, raising the gain will result in instabilities.  Also overshoot is typically not a pleasant driving style.

## Integral Gain

An integral gain addresses the problem when the state fails to converge to the target, especially when it has a constant offset.  Consider the case of an unmodeled force that causes the AV to drive off-center of its lane.  The integral error sums the error over time and a constant error will eventually result in a large corrective input.  Here is an example of adding `Ki=1` in addition to the `Kp=1` proportional term:

![Kp=1, Ki=1](.Images/pid110.png)

The yaw value avoids the asymptotic behavior, but does have overshoot, and can become unstable with too high an integral term.

## Derivative Gain

A derivative gain is proportional not to the difference between target and measurement, but proportional to how much this difference is changing over time.  If the target and measurement are diverging, a derivative gain will produce a positive corrective signal that drives the robot to close the gap.  Ideally this will enable faster target signal following.  On the other hand, when the target and measurement are converging, it will send a negative control that slows down convergence.  Ideally this will be sufficient to prevent overshoot.    For example, a `Kd=1` added to the case of `Kp=4` above, gives the following:

![Kp=4, Kd=1](.Images/pid401.png)

Here we get fast convergence and avoid overshoot.  Some challenges with derivative terms include that latency and noise can cause instability and oscillations.   As an example of this, observe what happens when we use the same parameters except that `--use_rate` if not selected:

![Kp=4, Kd=1, rate=0](.Images/pid401_r0.png)

The wobbles happen because the time derivative of *Error(t)* now is calculated fully numerically instead of using the yaw rate from the IMU.  When we do derivatives numerically they are susceptible to noise, and they may have some latency -- both problems that can cause instability with a controller.

# Line Follower

The above example controlled the yaw angle of the Turtlebot.  Now it is not hard to turn this into a line follower.  If we use computer vision techniques to determine the error between a target heading that would cause the robot to follow a line and the actual heading, then we simply use the PID controller to drive this error to zero while driving forward.

Now let's consider an image from the Turtlebot:

![Green line](.Images/green_line_image.png)

We can see the green line in it.  If we know the calibration parameters we can find the angle between the green line and the optical center and run a PID controller on this.  But there is an easier way.  Lets define our error to be the difference between the x-coordinate of the image center and the x-coordinate of the green line. This error will be roughly proportional to the angular heading error, and is certainly good enough for a PID controller.  

Now there are two important differences between this error measure and the one from the IMU.  The units are different, and so to get the same performance you may need to scale all the PID parameters accordingly.  Also the rate at which measurements are taken may be different (due to the frame rate of the camera) and latency may differ (due to processing you perform), both impacting feedback and stability.  It is a good idea to test your controller on the actual image measurements to tune its parameters accordingly.


___
### [Back to Index](../Readme.md)






