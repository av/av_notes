# State Machines

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [State Machines](#state-machines)
* [Create `vacbot` Package](#create-vacbot-package)
* [`vac_01_basic.py`: Basic State Machine](#vac_01_basic-py-basic-state-machine)
* [`vac_02_userdata.py`: State Machine with User Data](#vac_02_userdata-py-state-machine-with-user-data)
* [`vac_03_nested.py`: A Nested State Machine](#vac_03_nested-py-a-nested-state-machine)
* [`vac_reset.py`: Reposition the Turtlebot](#vac_reset-py-reposition-the-turtlebot)
* [`vac_04_arc_follow.py`: Simple Arc Following](#vac_04_arc_follow-py-simple-arc-following)
* [`vac_05_actions_waypoints.py`: Actions to Reach Waypoints](#vac_05_actions_waypoints-py-actions-to-reach-waypoints)
* [`vac_06_route_follow.py`: Dynamic Goals](#vac_06_route_follow-py-dynamic-goals)

___
# State Machines

State machines are a way to model and implement structured tasks.  This document supplements the State Machines lecture with directions for running the code as a ROS package.  All of the examples leverage the ROS SMACH library, described here: [http://wiki.ros.org/smach](http://wiki.ros.org/smach).  

# Create `vacbot` Package

Create a package to run the state machine demos called `vacbot` as follows:
```
$ cd ~/catkin_ws/src
$ catkin_create_pkg vacbot rospy
$ cd vacbot/src
$ cp <path_to_repo>/av_notes/AV/python/vac* .
```
Note, for all the examples after `vac_03`, you will need the [Green Line](https://gitlab.msu.edu/av/greenline) package added to your catkin workspace.


# `vac_01_basic.py`: Basic State Machine

The first example, `vac_01_basic.py`, is a simple, 2-state, state machine.  Each state is defined by a class that inherits the `smach.State` class.  You should define at least 2 functions: the `__init__()` and the `execute()` functions.  You can add additional variables and functions as in any standard Python class.  To run:
```
$ rosrun vacbot vac_01_basic.py
```
The code is:
```python
#!/usr/bin/env python
import rospy
import smach

class Charge(smach.State):  # Define a state and outcomes
    def __init__(self):
        smach.State.__init__(self, outcomes=['full'])

    def execute(self, userdata):
        rospy.loginfo('Charging')
        return 'full'

class Clean(smach.State):   # Define a state and outcomes
    def __init__(self):
        smach.State.__init__(self, outcomes=['done','cleaning','need_charge'])
        self.cleanval = 0
        self.time_since_charge = 0
    
    def execute(self, userdata):
        self.time_since_charge += 1

        if self.cleanval >= 3:
            return 'done'
        elif self.time_since_charge >= 3:
            rospy.loginfo('Need a charge')
            self.time_since_charge = 0
            return 'need_charge'
        else:
            self.cleanval += 1
            rospy.loginfo('Clean value: '+str(self.cleanval))
            return 'cleaning'


def init_vac_basic_sm():
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['finished','aborted'])
    sm.set_initial_state(['CLEAN'])

    with sm:  # This opens sm container for adding states:
        smach.StateMachine.add('CHARGE', Charge(),  # Add state and transition
                transitions={'full':'CLEAN'})
        smach.StateMachine.add('CLEAN', Clean(),    # Add state and transition
                transitions={'done':'finished',
                             'cleaning':'CLEAN',
                             'need_charge':'CHARGE'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('vac_01')
    rospy.sleep(0.5)         # Time to initialize to avoid missing log messages
    sm = init_vac_basic_sm() # Create state machine    
    outcome = sm.execute()   # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)
```
The output from running it is:
```
$ rosrun vacbot vac_01_basic.py
[ DEBUG ] : Setting initial states to ['CLEAN']
[ DEBUG ] : Adding state (CHARGE, <__main__.charge object at 0x7f8b35a2eb80>, {'full': 'CLEAN'})
[ DEBUG ] : Adding state 'CHARGE' to the state machine.
[ DEBUG ] : State 'CHARGE' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR CHARGE: {'full': 'CLEAN'}
[ DEBUG ] : Adding state (CLEAN, <__main__.clean object at 0x7f8b35a2ed30>, {'done': 'finished', 'cleaning': 'CLEAN', 'need_charge': 'CHARGE'})
[ DEBUG ] : Adding state 'CLEAN' to the state machine.
[ DEBUG ] : State 'CLEAN' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR CLEAN: {'done': 'finished', 'cleaning': 'CLEAN', 'need_charge': 'CHARGE'}
[  INFO ] : State machine starting in initial state 'CLEAN' with userdata:
        []
[INFO] [1615137005.226086]: Clean value: 1
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[INFO] [1615137005.229162]: Clean value: 2
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[INFO] [1615137005.232031]: Need a charge
[  INFO ] : State machine transitioning 'CLEAN':'need_charge'-->'CHARGE'
[INFO] [1615137005.233104]: Charging
[  INFO ] : State machine transitioning 'CHARGE':'full'-->'CLEAN'
[INFO] [1615137005.233979]: Clean value: 3
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[  INFO ] : State machine terminating 'CLEAN':'done':'finished'
[INFO] [1615137005.234937]: Final outcome: finished
```

# `vac_02_userdata.py`: State Machine with User Data

The second example illustrates storing user data in a state machine and accessing it and changing it from states within the state machine.  To run:
```
$ rosrun vacbot vac_02_userdata.py
```
Here is the code:
```python
#!/usr/bin/env python
import rospy
import smach

class Charge(smach.State): # Define a state, outcomes and userdata hooks
    def __init__(self):
        smach.State.__init__(self, outcomes=['full'], 
                            input_keys=['in_charge'], output_keys=['out_charge'])
        self.delta_charge = 2

    def execute(self, userdata):
        userdata.out_charge = userdata.in_charge + self.delta_charge
        rospy.loginfo('Charge value: '+str(userdata.in_charge)) # in_charge and out_charge are the same
        return 'full'

class Clean(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['done','cleaning','need_charge'],
                            input_keys=['in_charge','in_clean'], 
                            output_keys=['out_charge','out_clean'])
    
    def execute(self, userdata):
        if userdata.in_clean >= 3:
            return 'done'
        elif userdata.in_charge <= 0:
            return 'need_charge'
        else:
            userdata.out_clean = userdata.in_clean + 1
            userdata.out_charge = userdata.in_charge - 1
            rospy.loginfo('Clean: '+str(userdata.in_clean) + ', charge: ' + str(userdata.in_charge))
            return 'cleaning'


def init_vac_userdata():
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['finished','aborted'])
    sm.userdata.sm_clean = 0
    sm.userdata.sm_charge = 0
    sm.set_initial_state(['CLEAN'])

    with sm:
        smach.StateMachine.add('CHARGE', Charge(), # Add state and mapping for IO hooks
                transitions={'full':'CLEAN'},
                remapping={'in_charge':'sm_charge','out_charge':'sm_charge'})
        smach.StateMachine.add('CLEAN', Clean(),
                transitions={'done':'finished',
                             'cleaning':'CLEAN',
                             'need_charge':'CHARGE'},
                remapping={'in_charge':'sm_charge','out_charge':'sm_charge',
                           'in_clean':'sm_clean','out_clean':'sm_clean'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('vac_02')
    rospy.sleep(0.5)          # Time to initialize to avoid missing log messages
    sm = init_vac_userdata()  # Create state machine
    outcome = sm.execute()    # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)
```
Here is the result of running this state machine:
```
$ rosrun vacbot vac_02_userdata.py
[ DEBUG ] : Setting initial states to ['CLEAN']
[ DEBUG ] : Adding state (CHARGE, <__main__.charge object at 0x7fa35521cbe0>, {'full': 'CLEAN'})
[ DEBUG ] : Adding state 'CHARGE' to the state machine.
[ DEBUG ] : State 'CHARGE' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR CHARGE: {'full': 'CLEAN'}
[ DEBUG ] : Adding state (CLEAN, <__main__.clean object at 0x7fa35521cd90>, {'done': 'finished', 'cleaning': 'CLEAN', 'need_charge': 'CHARGE'})
[ DEBUG ] : Adding state 'CLEAN' to the state machine.
[ DEBUG ] : State 'CLEAN' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR CLEAN: {'done': 'finished', 'cleaning': 'CLEAN', 'need_charge': 'CHARGE'}
[  INFO ] : State machine starting in initial state 'CLEAN' with userdata:
        ['sm_clean', 'sm_charge']
[  INFO ] : State machine transitioning 'CLEAN':'need_charge'-->'CHARGE'
[INFO] [1615137689.237661]: Charge value: 2
[  INFO ] : State machine transitioning 'CHARGE':'full'-->'CLEAN'
[INFO] [1615137689.240201]: Clean: 1, charge: 1
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[INFO] [1615137689.241065]: Clean: 2, charge: 0
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[  INFO ] : State machine transitioning 'CLEAN':'need_charge'-->'CHARGE'
[INFO] [1615137689.241949]: Charge value: 2
[  INFO ] : State machine transitioning 'CHARGE':'full'-->'CLEAN'
[INFO] [1615137689.242765]: Clean: 3, charge: 1
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[  INFO ] : State machine terminating 'CLEAN':'done':'finished'
[INFO] [1615137689.243521]: Final outcome: finished
```
# `vac_03_nested.py`: A Nested State Machine
A state machine can be treated just like a state.  In this case, the state machine in `vac_01_basic.py` is added as a state, and the outputs are mapped as transitions.  To run:
```
$ rosrun vacbot vac_03_nested.py
```
The code is:
```python
#!/usr/bin/env python
import rospy
import smach
import time
from vac_01_basic import init_vac_basic_sm  #import state machine

class RandChoice(smach.State):
    '''Randomly returns a yes or no depending on the time of day'''
    def __init__(self):
        smach.State.__init__(self, outcomes=['yes','no'])

    def execute(self, userdata):       
        rospy.loginfo('Asking robot to clean my room') 
        if int( time.time() ) % 2:
            return 'yes'
        else:
            return 'no'

def init_vac_moody():
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['clean_room','dirty_room','aborted'])
    sm.set_initial_state(['ASK_VAC'])

    with sm:
        smach.StateMachine.add('ASK_VAC', RandChoice(),  # Add a state
                transitions={'yes':'DO_ROOM',
                             'no':'dirty_room'})
        smach.StateMachine.add('DO_ROOM', init_vac_basic_sm(), # Add a state machine
                transitions={'finished':'clean_room',
                             'aborted':'aborted'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('vac_03_moody')
    rospy.sleep(0.5)         # Time to initialize to avoid missing log messages
    sm = init_vac_moody()  # Create state machine
    outcome = sm.execute() # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)
```
Here is the output
```
$ rosrun vacbot vac_03_nested.py
[ DEBUG ] : Setting initial states to ['ASK_VAC']
[ DEBUG ] : Adding state (ASK_VAC, <__main__.RandChoice object at 0x7f2d0197dd60>, {'yes': 'DO_ROOM', 'no': 'dirty_room'})
[ DEBUG ] : Adding state 'ASK_VAC' to the state machine.
[ DEBUG ] : State 'ASK_VAC' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR ASK_VAC: {'yes': 'DO_ROOM', 'no': 'dirty_room'}
[ DEBUG ] : Setting initial states to ['CLEAN']
[ DEBUG ] : Adding state (CHARGE, <vac_01_basic.charge object at 0x7f2d0197df40>, {'full': 'CLEAN'})
[ DEBUG ] : Adding state 'CHARGE' to the state machine.
[ DEBUG ] : State 'CHARGE' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR CHARGE: {'full': 'CLEAN'}
[ DEBUG ] : Adding state (CLEAN, <vac_01_basic.clean object at 0x7f2d019a7070>, {'done': 'finished', 'cleaning': 'CLEAN', 'need_charge': 'CHARGE'})
[ DEBUG ] : Adding state 'CLEAN' to the state machine.
[ DEBUG ] : State 'CLEAN' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR CLEAN: {'done': 'finished', 'cleaning': 'CLEAN', 'need_charge': 'CHARGE'}
[ DEBUG ] : Adding state (DO_ROOM, <smach.state_machine.StateMachine object at 0x7f2d0197df10>, {'finished': 'clean_room', 'aborted': 'aborted'})
[ DEBUG ] : Adding state 'DO_ROOM' to the state machine.
[ DEBUG ] : State 'DO_ROOM' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR DO_ROOM: {'finished': 'clean_room', 'aborted': 'aborted'}
[  INFO ] : State machine starting in initial state 'ASK_VAC' with userdata:
        []
[INFO] [1615137857.478106]: Asking robot to clean my room
[  INFO ] : State machine transitioning 'ASK_VAC':'yes'-->'DO_ROOM'
[  INFO ] : State machine starting in initial state 'CLEAN' with userdata:
        []
[INFO] [1615137857.481953]: Clean value: 1
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[INFO] [1615137857.482910]: Clean value: 2
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[INFO] [1615137857.483718]: Need a charge
[  INFO ] : State machine transitioning 'CLEAN':'need_charge'-->'CHARGE'
[INFO] [1615137857.484506]: Charging
[  INFO ] : State machine transitioning 'CHARGE':'full'-->'CLEAN'
[INFO] [1615137857.485298]: Clean value: 3
[  INFO ] : State machine transitioning 'CLEAN':'cleaning'-->'CLEAN'
[  INFO ] : State machine terminating 'CLEAN':'done':'finished'
[  INFO ] : State machine terminating 'DO_ROOM':'finished':'clean_room'
[INFO] [1615137857.486094]: Final outcome: clean_room
```

# `vac_reset.py`: Reposition the Turtlebot

The utility `vac_reset.py` will reposition the Turtlebot to any of the pre-defined ellipses marked 1 to 6.  Notice that it uses `SimpleActionState`.  To run, call the following in separate terminals:
```
$ roslaunch greenline turtlebot3_greenline.launch
$ roslaunch greenline start_movebase.launch    
$ rosrun vacbot vac_reset.py  
```
The code is:
```python
#!/usr/bin/env python
import rospy
import smach
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler
from transform_frames import transform_frames

class Waypoints:
    def __init__(self):
        self.trans = transform_frames()

    def get_line_waypoints(self, x, y, yaw): 
        wplist = PoseArray()
        pnt = Point(x, y, 0.)
        quat = Quaternion(*(quaternion_from_euler(0, 0, yaw, axes='sxyz')))
        wplist.poses.append(Pose(pnt, quat))
        wplist_map = self.trans.pose_transform(wplist, 'odom', 'map')
        return wplist_map

def init_follower_sm(wplist):
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded', 'aborted', 'preempted'])
    # These three outcomes are standard for action states, and are the outcomes of SimpleActionState

    with sm:  # This opens sm container for adding states:
        for i, wp in enumerate(wplist.poses):
            wpose = MoveBaseGoal()
            wpose.target_pose.header.frame_id = "map"
            wpose.target_pose.header.stamp = rospy.Time.now()
            wpose.target_pose.pose = wp
            name = 'wp' + str(i)
            if i < len(wplist.poses) - 1:
                nextname = 'wp' + str(i + 1)
            else:
                nextname = 'succeeded'
            smach.StateMachine.add(name,
                                   SimpleActionState('move_base',
                                                     MoveBaseAction,
                                                     goal=wpose,
                                                     server_wait_timeout=rospy.Duration(10.0)),
                                   transitions={'succeeded': nextname})
    return sm

class ReadOdom:
    ''' This class subscribes to /odom and makes it available as self.pose'''

    def __init__(self):
        self.pose = Pose()
        rospy.Subscriber("/odom", Odometry, self.callback)
        rospy.sleep(0.1)  # Make sure pose has time to be initialized with callback
        rospy.loginfo('Subscribing to /odom')

    def callback(self, msg):
        '''Copies odometry pose to self.pose'''
        self.pose = msg.pose.pose

if __name__ == '__main__':

    rospy.init_node('runReset', anonymous=True)
    ro = ReadOdom()
    wp = Waypoints()
    keepgoing = True
    while keepgoing:
        ans = int(input("Choose a spot (1,2,3,4,5,6): "))
        if ans == 1:
            wplist = wp.get_line_waypoints(-1.5, 1.5, 0.)
        elif ans == 2:
            wplist = wp.get_line_waypoints(-0.7, 1.5, -0.1)
        elif ans == 3:
            wplist = wp.get_line_waypoints(0.25, 1.05, -1.7)
        elif ans == 4:
            wplist = wp.get_line_waypoints(0.0, -0.58, -1.6)
        elif ans == 5:
            wplist = wp.get_line_waypoints(0, -1.58, 0.1)
        elif ans == 6:
            wplist = wp.get_line_waypoints(0.78, -1.6, 0.1)
        elif ans == -100:
            wplist = wp.get_line_waypoints(1.5, -0.5, 0.)  # drive into red region (test)
        else:
            print("Not one of the number 1-6, quitting")
            keepgoing = False
            continue
        sm = init_follower_sm(wplist)  # Create state machine
        outcome = sm.execute()  # Execute state machine
        rospy.loginfo('Final outcome: ' + outcome)
```

# `vac_04_arc_follow.py`: Simple Arc Following
One way a state can control the Turtlebot is by publishing commands to the `/cmd_vel` topic, as illustrated by `vac_04_arc_follow.py`.  A disadvantage of this is that it will not avoid obstacles and so could become stuck.  Notice that it includes a test for being stuck that result in returning `abort`. Run with:
```
$ roslaunch greenline turtlebot3_greenline.launch
$ roslaunch greenline start_movebase.launch    
$ rosrun vacbot vac_04_arc_follow.py  
```
Code is here:
```python
#!/usr/bin/env python
import rospy
import smach
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray, Twist
from transform_frames import transform_frames
from nav_msgs.msg import Odometry
import numpy as np

def get_target():  # Here target is hard-coded in odom coordinates
    target = PoseArray()
    target.poses.append(Pose(Point( 0., -0.5, 0.),Quaternion(0.,0.,0.,1.)))    
    return target

class IsStuck():
    ''' Tests if robot is stuck '''
    def __init__(self):
        self.nstuck = 0
    def test(self, cmd_speed):
        try:
            msg = rospy.wait_for_message("odom", Odometry, 0.2)
            if np.abs(msg.twist.twist.linear.x) < np.abs(cmd_speed) / 100:
                self.nstuck += 1
            else:
                self.nstuck = 0
        except rospy.ROSException:
            pass
        if self.nstuck >= 10:
            return True
        else:
            return False

class ArcDrive(smach.State): 
    def __init__(self):
        smach.State.__init__(self, outcomes=['keepgoing','done','aborted'],
                                   input_keys=['in_target'])
        self.trans = transform_frames()
        self.move_pub = rospy.Publisher('cmd_vel', Twist, latch=True, queue_size=1)  
        self.stuck = IsStuck()

    def execute(self, userdata):
        #Find target location relative to current location in base_footprint
        target = self.trans.pose_transform(userdata.in_target, 'odom', 'base_footprint' )
        targetPoint = target.poses[0].position
        vel = Twist()
        keepgoing = targetPoint.x > 0 #Check if target is in front of robot
        if keepgoing:  # Only command a non-zero velocity if target is in front of robot
            vel.linear.x = 0.1  #Use fixed linear velocity
            k = 2.*targetPoint.y / (targetPoint.x**2 + targetPoint.y**2)  #Eq. from lecture 7
            if abs(k) > 5.:  # Prevent turning radius < 0.2m for smoother trajectories
                k = 5. * k/abs(k)
            vel.angular.z = k * vel.linear.x  # omega = k * v
        self.move_pub.publish(vel)
        rospy.sleep(0.2)        
        if self.stuck.test(vel.linear.x):
            return 'aborted'            
        elif keepgoing:
            return 'keepgoing'
        else:
            return 'done'

def init_arcdrive_sm():
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded','aborted'])
    sm.userdata.sm_target = get_target()  #Target in odom coords
    sm.set_initial_state(['DRIVE'])
    
    with sm:  # This opens sm container for adding states:
        smach.StateMachine.add('DRIVE', ArcDrive(), # Add state and mapping for IO hooks
                transitions={'keepgoing':'DRIVE','done':'succeeded','aborted':'aborted'},
                remapping={'in_target':'sm_target'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('Arc_Driving_SMACH')
    sm = init_arcdrive_sm( ) # Create state machine    
    outcome = sm.execute()   # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)
```
___
# `vac_05_actions_waypoints.py`: Actions to Reach Waypoints
Rather than directly publishing `/cmd_vel`, it is possible to use a `SimpleActionState` to give a `MoveBaseAction` with a goal pose, and the Turtlebot will attempt to reach the target pose.  Note: only basic short-range planning is enabled for this.  To run:
```
$ roslaunch greenline turtlebot3_greenline.launch
$ roslaunch greenline start_movebase.launch   
$ rosrun vacbot vac_05_actions_waypoints.py
```
The code is here:
```python
#!/usr/bin/env python
import rospy
import smach
import math
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler

def get_line_waypoints():  #A list of pre-defined waypoints
    wplist = PoseArray()
    pnt1 = Point( -1., 1.6, 0.)
    quat1 = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt1,quat1))
    pnt2 = Point( 0.5, 1.1, 0.)
    quat2 = Quaternion(*(quaternion_from_euler(0, 0, -90.*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt2,quat2))
    pnt3 = Point( 0., -0.5, 0.)
    quat3 = Quaternion(*(quaternion_from_euler(0, 0, -90*math.pi/180, axes='sxyz')))
    wplist.poses.append(Pose(pnt3,quat3))    
    return wplist


def init_follower_sm(wplist):
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded','aborted','preempted'])
    # These three outcomes are standard for action states, and are the outcomes of SimpleActionState

    with sm:  # This opens sm container for adding states:
        for i,wp in enumerate(wplist.poses):
            wpose = MoveBaseGoal()
            wpose.target_pose.header.frame_id = "map"
            wpose.target_pose.header.stamp = rospy.Time.now()
            wpose.target_pose.pose = wp
            name = 'wp'+str(i)
            if i < len(wplist.poses)-1:
                nextname = 'wp'+str(i+1)
            else:
                nextname = 'succeeded'
            smach.StateMachine.add( name, 
                                    SimpleActionState( 'move_base', 
                                                       MoveBaseAction,
                                                       goal=wpose,
                                                       server_wait_timeout=rospy.Duration(10.0)),
                                    transitions={'succeeded':nextname,'aborted':nextname,'preempted':'preempted'} )
    return sm
    
if __name__ == '__main__':
    rospy.init_node('Basic_SMACH')
    wplist = get_line_waypoints()
    sm = init_follower_sm( wplist ) # Create state machine    
    outcome = sm.execute()   # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)
```
___
# `vac_06_route_follow.py`: Dynamic Goals
In `vac_05_actions_waypoints.py` the goals were fixed once the state machine was created.  It is possible to have dynamic goals that can be changed just before the `SimpleActionState` is called.  The trick is to give a callback that specifies the goal, rather than a fixed goal, as shown below.  To run:
```
$ roslaunch greenline turtlebot3_greenline.launch
$ roslaunch greenline start_movebase.launch   
$ rosrun vacbot vac_06_route_follow.py  
```
The code is here;
```python
import math
import rospy
import smach
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from vac_04_arc_follow import init_arcdrive_sm

    
class SetWaypointBehindWall(smach.State):
    '''This should find the wall that the robot is facing and publish a waypoint 0.5m behind it '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['set_wp'],output_keys=['out_behind_wall_goal'])

    def execute(self, userdata):
        # This uses a fixed waypoint, but for the machinery below would work with a dynamically set waypoint
        pnt = Point( 0., -1.6, 0.)
        quat = Quaternion(*(quaternion_from_euler(0, 0, 0.*math.pi/180, axes='sxyz')))
        wp = Pose(pnt,quat)
        wpose = MoveBaseGoal()
        wpose.target_pose.header.frame_id = "map"
        wpose.target_pose.header.stamp = rospy.Time.now()
        wpose.target_pose.pose = wp
        userdata.out_behind_wall_goal = wpose
        return 'set_wp'

def init_circumnavigate_wall_sm():
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])
    sm.set_initial_state(['SET_POINT'])
    sm.userdata.sm_goal = MoveBaseGoal()

    with sm:  
        #First create a state to find point behind wall and store it in userdata:
        smach.StateMachine.add('SET_POINT', SetWaypointBehindWall(),
                transitions={'set_wp':'DRIVE_GOAL'},
                remapping={'out_behind_wall_goal':'sm_goal'} )
        #We will not know the position behind the wall until we get to the wall
        #So use a callback to get it from userdata
        def tbot_goal_cb(userdata, goal ):
            return userdata.behind_wall  #This can access userdata if input_keys are set in the SimpleActionState below
        smach.StateMachine.add( 'DRIVE_GOAL', 
                                SimpleActionState( 'move_base', 
                                                    MoveBaseAction,
                                                    goal_cb=tbot_goal_cb,  #set a callback
                                                    input_keys=['behind_wall'],
                                                    server_wait_timeout=rospy.Duration(10.0)),
                                transitions={'succeeded':'succeeded','preempted':'preempted','aborted':'aborted'},
                                remapping={'behind_wall':'sm_goal'} )
    return sm

def navigate_course_sm():
    '''Combines state machines into a complete state machine for the course
       For now, just a start at a solution'''
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded','preempted','aborted'])
    sm.set_initial_state(['LINE_FOLLOWER'])

    with sm:  #This opens sm container for adding states:
        smach.StateMachine.add('LINE_FOLLOWER', init_arcdrive_sm(),
                transitions={'succeeded':'WALL_CIRCUMNAV'})
        smach.StateMachine.add('WALL_CIRCUMNAV', init_circumnavigate_wall_sm(),
                               transitions={'succeeded':'succeeded','preempted':'preempted','aborted':'aborted'} )
    return sm
    
    
if __name__ == '__main__':
    rospy.init_node('Simplified_Course')
    sm = navigate_course_sm()
    outcome = sm.execute()
    rospy.loginfo('Final outcome: '+outcome)
```

___
### [Back to Index](../Readme.md)






