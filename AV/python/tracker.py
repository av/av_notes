#!/usr/bin/env python
"""
   Simple tracking example with a Kalman Filter with state model: [x,y,vx,vy]
   Incrementally plots position
   Uses pykalman package see: https://pykalman.github.io/
   To install pykalman on your laptop:
     source ~/envs/work/bin/activate
     pip install pykalman scipy 

   Daniel Morris, 2020
"""
__author__="Daniel morris"
import numpy as np
import matplotlib.pyplot as plt
from pykalman import KalmanFilter   # Install pykalman as described above
from state_hist import state_hist   # From state_hist.py
import argparse

class tracker:

    def __init__(self, maxHist):
        ''' Initialize state vector, measurement model, and noise model'''
        self.time = 0.
        self.tmax = 10.0
        self.dt = 0.1               # Measurement rate 
        self.Rparams = [0.05, 0.02] # Used to create random measurement covariance for the simulator
        self.set_R()                # Sets random covariance matrix for measurement / observation
        self.Q = np.diag([0.,0.,1e-2,1e-2])         # System noise (accounts for trueStateOffsets)
        self.trueState   = np.array([0.,0.,1.,0.5]) # For the simulator we need a true state
        self.trueStateOffsets = np.array([0.,0.,-0.01,0.01])  # Unmodeled offsets to true state per frame
        self.stateTrans  = [[1., 0.,self.dt,0.], [0., 1., 0., self.dt], [0.,0.,1.,0], [0.,0.,0.,1.]]
        self.H           = [[1.,0.,0.,0],[0.,1.,0.,0.]]  #Transforms state to measurement
        self.z           = []              # Measurement
        self.estState    = np.zeros((4,))  # Initial state
        xysigma = 0.1      # Uncertainty in xy position
        vsigma = 1.0       # Uncertainty in velocity
        self.estCov      = np.diag([xysigma**2,xysigma**2,vsigma**2,vsigma**2])  # Initial covariance
        self.predState   = []
        self.predCov     = []

        self.kf = KalmanFilter( 
            initial_state_mean = self.estState,
            initial_state_covariance = self.estCov,
            transition_matrices = self.stateTrans,
            transition_covariance = self.Q,  #Q
            observation_matrices = self.H   #H
            )
        
        self.hist = state_hist(self.dt, maxHist=maxHist) # Storing and plotting data
        self.hist.append(self)          # Store initial state

    def step(self):
        '''One step consisting of:
        Simulated motion + simulated measurement with added noise
        State prediction and update using noisy measurement
        '''
        self.time += self.dt
        self.trueState = np.dot(self.stateTrans, self.trueState + self.trueStateOffsets)
        self.set_R()     # create new measurement covariance
        self.z = np.dot(self.H, self.trueState)  + np.dot( self.RC, np.random.randn( 2,))  # measurement

        # With no measurement, this is just the prediction (only need to do this for plotting prediction)
        self.predState,  self.predCov  = self.kf.filter_update( self.estState, self.estCov )

        # Do full prediction and update of state with measurement:
        self.estState,  self.estCov  = self.kf.filter_update( self.estState, self.estCov, 
            observation=self.z, observation_covariance=self.R)

    def set_R(self):
        '''Set random measurement covariance
        self.Rparams: 1x2 [random sigma value, base sigma value] applied to each dimension
        '''        
        t = np.random.rand(1)[0] * 100.0
        rot = np.array([[np.cos(t), np.sin(t)],[-np.sin(t),np.cos(t)]])
        diagmat = np.diag( self.Rparams[0]*np.random.rand(2,) + self.Rparams[1] )
        self.RC = np.dot(rot, diagmat ) #left half of covariance
        self.R = np.dot( self.RC, self.RC.T)

    def run(self):

        keepgoing = True
        while keepgoing and self.time <= self.tmax:

            self.step()            
            keepgoing = self.hist.append( self )    #store and plot data
            


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='KF Tracker')
    parser.add_argument('--maxHist', type=int, default=20, help='Maximum history')
    parser.add_argument('--use_rate', action='store_true', default=False, help='Use IMU yaw rate')
    
    args, unknown = parser.parse_known_args()  # For roslaunch compatibility
    if unknown: print('Unknown args:',unknown)

    sim = tracker(args.maxHist)
    sim.run()

