#!/usr/bin/env python
'''
    param_demo.py 

    A simple example of getting and setting ROS parameters

    Daniel Morris, Nov 2020
'''
import rospy

if __name__== '__main__':
    rospy.init_node('move')

    my_scalar = rospy.get_param('/my_scalar',100.0)  # Get parameter and if does not exist default is 100.0
    print(f'/my_scalar {my_scalar}')
    my_dict = rospy.get_param('/my_dict')            # Lists are just as easy to work with as scalars
    print(f'/my_dict {my_dict}')

    if not rospy.has_param('/new_list'):             # Can check if a parameter exists before setting it
        rospy.set_param('/new_list',['A','new','list'])

    new_list = rospy.get_param('/new_list')
    print(f'/new_list {new_list}')

