#!/usr/bin/env python
"""
   state_hist.py

   Stores states and plots both position history and current state with uncertainty
   Used by tracker.py 

   Daniel Morris, 2020
"""
__author__="Daniel Morris"
import numpy as np
import matplotlib.pyplot as plt

class state_hist:
    '''Store history of positions and velocities for plotting'''

    def __init__(self, dt, maxHist=5):
        self.dt = dt
        self.meas = [[],[]]
        self.pred = [[],[]]
        self.predVel = [[],[]]
        self.est = [[],[]]
        self.estVel = [[],[]]
        self.measCov = []
        self.predCov = []
        self.estCov = []
        self.maxHist = maxHist  #store at most this number of historical states
        self.frame = -1
        self.firstPlot = True

    def append(self, sim, doPlot=True, doInteractive=True):
        '''Provide sim class that includes the various states and measurements'''
        if hasattr(sim,'trueState'):
            if not hasattr(self,'true'):
                self.true = [[],[]]     #make these optional as when not simulating won't have them
                self.trueVel = [[],[]]
            self.true[0].append(sim.trueState[0])
            self.true[1].append(sim.trueState[1])
            self.trueVel[0].extend( self.getVelX(sim.trueState) )
            self.trueVel[1].extend( self.getVelY(sim.trueState) )
        if len(sim.z):         #only append z when we have measurements
            self.meas[0].append(sim.z[0])
            self.meas[1].append(sim.z[1])
        if len(sim.predState): #only append predState when we have one:
            self.pred[0].append(sim.predState[0])
            self.pred[1].append(sim.predState[1])       
            self.predVel[0].extend( self.getVelX(sim.predState) )
            self.predVel[1].extend( self.getVelY(sim.predState) )
        self.est[0].append(sim.estState[0])
        self.est[1].append(sim.estState[1])   
        self.estVel[0].extend( self.getVelX(sim.estState) )
        self.estVel[1].extend( self.getVelY(sim.estState) )

        if len(sim.z):
            self.measCov = sim.R
        if len(sim.predState):
            self.predCov = sim.predCov
        self.estCov = sim.estCov

        self.frame += 1
        self.pruneOld()

        if doPlot:
            keepgoing = self.plot(doInteractive)
        return keepgoing

    def pruneOld(self):
        '''Prune history older than maxHist'''
        if hasattr(self,'true') and len(self.true[0])>self.maxHist:
            self.true[0] = self.true[0][1:]
            self.true[1] = self.true[1][1:]        
            self.trueVel[0] = self.trueVel[0][3:]
            self.trueVel[1] = self.trueVel[1][3:]        
        if len(self.meas[0])>self.maxHist:
            self.meas[0] = self.meas[0][1:]
            self.meas[1] = self.meas[1][1:]
        if len(self.pred[0])>self.maxHist:
            self.pred[0] = self.pred[0][1:]
            self.pred[1] = self.pred[1][1:]
            self.predVel[0] = self.predVel[0][3:]
            self.predVel[1] = self.predVel[1][3:]       
        if len(self.est[0])>self.maxHist:
            self.est[0] = self.est[0][1:]
            self.est[1] = self.est[1][1:]        
            self.estVel[0] = self.estVel[0][3:]
            self.estVel[1] = self.estVel[1][3:]        
         


    def getVelX(self, state):
        return [state[0], state[0]+self.dt*state[2], np.nan]    

    def getVelY(self, state):
        return [state[1], state[1]+self.dt*state[3], np.nan]    
    
    def xyCov(self, state, cov):
        if len(cov):
            L = np.linalg.cholesky( cov[0:2,0:2] )
            st = np.sin(np.linspace(0, 2*np.pi, 100))
            ct = np.cos(np.linspace(0, 2*np.pi, 100))
            x = state[0][-1] + L[0,0]*ct + L[0,1]*st  #ellipse around latest x value
            y = state[1][-1] + L[1,0]*ct + L[1,1]*st  #ellipse around latest y value
            return x, y
        else:
            return [], []


    def plot(self, doInteractive):
        if self.firstPlot:
            if hasattr(self,'true'):
                plt.plot(self.true[0],    self.true[1],    color=(0.,1.,0.),   marker='s', linestyle='', label="True")
                plt.plot(self.trueVel[0], self.trueVel[1], color=(0.,1.,0.),   marker='',  linestyle='--' )
            plt.plot(self.pred[0],    self.pred[1],    color=(0.,0.,1.),   marker="v", linestyle='', label="Predicted")
            plt.plot(self.predVel[0], self.predVel[1], color=(0.,0.,1.),   marker="",  linestyle='-' )
            plt.plot(self.meas[0],    self.meas[1],    color=(1.,0.,0.),   marker='P', linestyle='--', label="Measured")
            plt.plot(self.est[0],     self.est[1],     color=(0.8,0.0,0.8), marker="^", linestyle='--', label="Estimated")
            plt.plot(self.estVel[0],  self.estVel[1],  color=(0.8,0.0,0.8), marker="",  linestyle='-' )

            x,y = self.xyCov(self.meas, self.measCov)
            plt.plot( x, y, color=(1.,0.,0.), linestyle='-')
            x,y = self.xyCov(self.pred, self.predCov)
            plt.plot( x, y, color=(0.,0.,1,), linestyle='-')
            x,y = self.xyCov(self.est, self.estCov)
            plt.plot( x, y, color=(0.8,.0,.8), linestyle='-')

            plt.legend()
            plt.gca().set_aspect('equal','box')
            plt.show(block=False)
            plt.title('Initial Pose')
            self.firstPlot = False
        else:
            if hasattr(self,'true'):
                plt.gca().lines[0].set_xdata(self.true[0])
                plt.gca().lines[0].set_ydata(self.true[1])
                plt.gca().lines[1].set_xdata(self.trueVel[0])
                plt.gca().lines[1].set_ydata(self.trueVel[1])
                i=2
            else:
                i=0
            plt.gca().lines[i+0].set_xdata(self.pred[0])
            plt.gca().lines[i+0].set_ydata(self.pred[1])
            #plt.gca().lines[i+1].set_xdata(self.predVel[0])
            #plt.gca().lines[i+1].set_ydata(self.predVel[1])
            plt.gca().lines[i+2].set_xdata(self.meas[0])
            plt.gca().lines[i+2].set_ydata(self.meas[1])
            plt.gca().lines[i+3].set_xdata(self.est[0])
            plt.gca().lines[i+3].set_ydata(self.est[1])
            plt.gca().lines[i+4].set_xdata(self.estVel[0][-3:])
            plt.gca().lines[i+4].set_ydata(self.estVel[1][-3:])
            x,y = self.xyCov(self.meas, self.measCov)
            plt.gca().lines[i+5].set_xdata(x)
            plt.gca().lines[i+5].set_ydata(y)
            x,y = self.xyCov(self.pred, self.predCov)
            plt.gca().lines[i+6].set_xdata(x)
            plt.gca().lines[i+6].set_ydata(y)
            x,y = self.xyCov(self.est, self.estCov)
            plt.gca().lines[i+7].set_xdata(x)
            plt.gca().lines[i+7].set_ydata(y)
            plt.gca().relim() 
            plt.gca().autoscale_view()
            plt.gca().set_aspect('equal','box')
            plt.title('Frame: '+str(self.frame))
            plt.draw()
            plt.pause(0.01)

        if doInteractive:
            keepgoing = input("Press Enter or q: ") 
            if keepgoing=='q':
                return False
        return True

