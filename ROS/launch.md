# ROS Launch and Params

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [Launching Nodes](#launching-nodes)
* [Example Launch Files](#example-launch-files)
  * [Launch File for `secrets`](#launch-file-for-secrets)
  * [A Turtlebot Launch File](#a-turtlebot-launch-file)
* [ROS Parameters](#ros-parameters)
  * [Command Line Parameters](#command-line-parameters)
  * [`rospy` Parameters](#rospy-parameters)

___
# Launching Nodes

So far we have started a node using the `rosrun` terminal command as follows:
```
$ rosrun <package_name> <node_name>
```
However, if you need to run many nodes simultaneously, starting each in its own terminal becomes ponderous.  The alternative, which addresses this problem, is to use a launch file.  This is started in a similar way:
```
$ roslaunch <package_name> <launch_file>
```
or else directly:
```
$ roslaunch <full_path_to_launch_file>
```

Some of the advantages in using a launch file are:

* A launch file can start as many nodes or other launch files as you like
* Parameters and arguments can be set inside a launch file
* It is easy to create a launch file that will initiate all the nodes you need for a particular task
* If `roscore` is not already running, it will be started by the launch file

A possible downside of launch files is that you no longer see standard output from your code (i.e. print statements).  This is one of the reasons for using the `rospy.rosinfo()` command within your code, as you can view these messages regardless of how your node is running.

___
# Example Launch Files

## Launch File for `secrets`

We can learn about launch files from some examples.  A launch file will have the suffix `.launch` and is conventionally stored in the `<package_folder>/launch` folder.  Let's create a launch file for the `secrets` package created in the [Threads](threads.md) document.  To do this do the following
```
$ cd ~/catkin_ws/src/secrets
$ mkdir launch
$ cd launch
$ cat > secrets.launch
```
Then press Enter and paste the following text:   
````
<launch>
  <node name="sender" pkg="secrets" type="secrets_pub.py" />
  <node name="finder" pkg="secrets" type="secrets_sub.py" />
</launch>
````
When done press Enter again and then ctrl-D.   This will create `secrets.launch`.  You can now start both the publish and subscribe nodes with the command:
```
$ roslaunch secrets secrets.launch
```
Try this.  It should start up both nodes and display the secret messages.  It will also start `roscore` if is it not already running.  

Next, let's examine the contents of the launch file.  It is written in YAML.  Each line starting with `<node ... />` starts a ROS node.  The `name="sender"` argument sets the node name to "sender".  You can see this by typing `rosnode list` in another terminal.  I get the following:
```
$ rosnode list
/finder
/rosout
/sender
```
Notice `/finder` and `/sender` node names.  The launch file names override the node names in the python code.  

The `pkg="secrets"` argument indicates the package name, and the `type="secrets_pub.py` gives the node executable to run.  This launch file runs two ROS nodes, but any number of nodes and launch files could be started by it.

## A Turtlebot Launch File

Next let's explore one of the Turtlebot launch files, which has some additional components.  First let's launch it with:
```
$ roslaunch turtlebot3_bringup turtlebot3_model.launch model:=burger
```
And you should see something like this:

![Turtlebot launch](.Images/turtlebot_bringup.png)

If you get an error, look at the warnings provided and one of the following might solve it:

* Make sure your X-server is running and you are able to display graphics from your command line.
* Make sure the environment variable `TURTLEBOT3_MODEL` is set.  You can set it with `export TURTLEBOT3_MODEL=burger`.
* If Rviz is crashing and you are on WSL, you can set `export LIBGL_ALWAYS_INDIRECT=`  (and actually you may want to add this to your .bashrc file).

To find this launch file, we need to first locate the `turtlebot3_bringup` package.  This is easy to do with the `roscd` command:
```
$ roscd turtlebot3_bringup
av@ROSVM01:/opt/ros/noetic/share/turtlebot3_bringup$
```

Now have a look at the launch file which is located within this package: `/opt/ros/noetic/share/turtlebot3_bringup/launch/turtlebot3_model.launch`.  
```
<launch>
  <arg name="model" default="$(env TURTLEBOT3_MODEL)" doc="model type [burger, waffle, waffle_pi]"/>
  <arg name="multi_robot_name" default=""/>

  <include file="$(find turtlebot3_bringup)/launch/turtlebot3_remote.launch">
    <arg name="model" value="$(arg model)" />
    <arg name="multi_robot_name" value="$(arg multi_robot_name)"/>
  </include>

  <node pkg="joint_state_publisher" type="joint_state_publisher" name="joint_state_publisher">
    <param name="use_gui" value="true"/>
    <param name="rate" value="50"/>
  </node>

  <node name="rviz" pkg="rviz" type="rviz" args="-d $(find turtlebot3_description)/rviz/model.rviz"/>
</launch>
```

This launch file illustrates a number of useful components

## Launch File Arguments and Defaults
We can see it starts by defining arguments with default values.  We passed in `model:=burger` as an argument, and if we didn't, it would get this from the `TURTLEBOT3_MODEL` environment variable.

## Calling other Launch Files
The block starting `<include ...>` calls another launch file and passes in arguments to it.  In this case it calls the `turtlebot3_remote.launch` file.  

## Find Command
The above include statement specifies the full path to the launch file.  It does this with a `find` command that locates a given package containing the launch file.  You can use the `find` command to find absolute paths to files within any ROS package.  For example, say you have an image located in `my_package/images/my_image.png`, where `my_package` is your ROS package.  You can refer to this image within a launch file with: `"$(find my_package)/images/my_image.png"`.  

The `find` command is quite importand for launch files since when you pass in arguments to packages, you need to give them the full pathnames.  This is illustrated in the next line which calls `rviz`.

## Run Package with Arguments
The final command line starts the `rviz` node, and passes in the Turtlebot model file as an argument, which is what you see in the image.  Notice the `args=` argument to the `node` line.  This shows how a launch file can pass arguments to a node if it is configured to accept arguments as explained in [Turtlebots_Gazebo](Turtlebots_Gazebo).  It also uses a `find` command to specify the exact location.

___
# ROS Parameters

ROS provides a parameter server which enables nodes to exchange configuration information.  This allows a user or a node to adjust the operation of running nodes without having to restart them.  Parameters are not intended for high-performance operations such as data transfer.  But they are useful for making changes on the fly nodes.  

For example, if you have an object detector running as a ROS node, you could create a ROS parameter called `/obj_threshold` which the detector node would periodically check and use to update its internal threshold.  In this way it is simple to adjust the operation of the object detector node without restarting it.

## Command Line Parameters

Documentation for the parameter server is available at [http://wiki.ros.org/Parameter Server](#http://wiki.ros.org/Parameter%20Server). Parameters can be scalars, lists or dictionaries.  Access from the command line is via the `rosparam` command which is dicumented here: [http://wiki.ros.org/rosparam](#http://wiki.ros.org/rosparam)  Here are a couple examples of its use from the command line:
```
$ rosparam set /my_scalar 25.0
$ rosparam set /my_list "['2', 4, 8.0]"
$ rosparam set /my_dict "{key1: 25.0, key2: 11.0, key3: 2.0}"
```
Notice that the three different types in the `/my_list` parameter list.  The dictionary `/my_dict` has three elements or sub-parameters.  We can see all the parameters with `rosparam list` as follows:
```
$ rosparam list
/my_dict/key1
/my_dict/key2
/my_dict/key3
/my_list
/my_scalar
/rosdistro
/rosversion
/run_id
```

## `rospy` Parameters

Parameters are accessible and setable within nodes.  Documentation is here [http://wiki.ros.org/rospy/Overview/Parameter%20Server](#http://wiki.ros.org/rospy/Overview/Parameter%20Server).  An example node [av_notes/ROS/python/param_demo.py](python/param_demo.py) that gets and sets parameters is as follows:
```python
import rospy

if __name__== '__main__':
    rospy.init_node('move')

    my_scalar = rospy.get_param('/my_scalar',100.0)  # Get parameter and if does not exist default is 100.0
    print(f'/my_scalar {my_scalar}')
    my_dict = rospy.get_param('/my_dict')            # Lists are just as easy to work with as scalars
    print(f'/my_dict {my_dict}')

    if not rospy.has_param('/new_list'):             # Can check if a parameter exists before setting it
        rospy.set_param('/new_list',['A','new','list'])

    new_list = rospy.get_param('/new_list')
    print(f'/new_list {new_list}')
```
After setting the ROS parameters in the previous section, I created a package called `params`, copied this code into `params/src`. Then running this node we get:
```
$ rosrun params param_demo.py
/my_scalar 25.0
/my_dict {'key1': 25.0, 'key2': 11.0, 'key3': 2.0}
/new_list ['A', 'new', 'list']
```
As expected, our ROS node is able to get and set parameters.  

___
### [Back to Index](../Readme.md)






