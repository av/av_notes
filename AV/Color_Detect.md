# Color-Based Object Detection

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [Logistic Regression](#logistic-regression)
  * [Goal](#goal)
  * [Results](#results)
  * [Code Explanation](#code-explanation)
  * [How Will Logistic Regression Be Used?](#how-will-logistic-regression-be-used)
  * [Can Detection Be Improved?](#can-detection-be-improved)
___
# Logistic Regression

This is a worked example of using logistic regression for color-based object detection.  The theory is explained in the lecture.

## Goal
Let's say we want to learn to detect green spheres in colored images.  This is a task that a student of mine has been doing for her research on automated sensor calibration.  Here is one sample from her dataset.  It is an image containing the sphere plus a mask that labels sphere pixels:
<p align="middle">
<img src=".Images/sphere1_img.png" width="300">
<img src=".Images/sphere1_seg.png" width="300">
</p>

## Results
Now out goal is to learn from this (and actually from many such images) and be able to automatically classify sphere pixels.  The result we are looking for is like that below on a test image:
<p align="middle">
<img src=".Images/sphere2_img.png" width="300">
<img src=".Images/prob_sphere2_img.png" width="300">
</p>
<p align="middle">
<img src=".Images/scoring_sphere2_img.png" width="300">
<img src=".Images/target_sphere2_img.png" width="300">
</p>

Top left is a new image that we will run our trained model on.  Top right is the output of our method; bright means high probability of being on a sphere.  Bottom left shows *True Positives*, *False Positives* and *False Negatives* (outlined below).  In the bottom right we can extract the largest contiguous region and call that our target, and plot its centroid as a red circle.

The pixel classification results are: 
* *True Positives*: (Green) pixels that are classified as target pixels and actually are target pixels.
* *False Positives*: (Red) pixels that are classified as target pixels but are actually background.
* *False Negatives*: (Blue) pixels that are classified as background but are actually targets.
* *True Negatives*: (Grayscale) pixels that are classified as background and are background.  These are often the majority of the pixels, often easily classified, and not part of a precision recall curve.

These terms are explained in the lecture, and make sure you understand how each is determined.

## Code Explanation
The code for the above is [av_notes/AV/python/logist_reg.py](python/logist_reg.py).  Let's see how it is done.  The main component is a class I call `LogisticReg`, defined as:
```python
class LogisticReg:
    def __init__(self ):
        ''' Initialize class with zero values '''
        self.cvec = np.zeros( (1,3) )        
        self.intercept = np.zeros( (1,) )

    def set_model(self, cvec, intercept):
        ''' Set model parameters manually
            cvec:      np.array([[p1,p2,p3]])
            intercept: np.array([p4])
        '''
        self.cvec = cvec
        self.intercept = intercept

    def fit_model_to_files(self, img_name, mask_name, exmask_name=''):
        ''' Load images from files and fit model parameters '''
        img = cv2.imread( img_name )
        mask = imread_channel( mask_name )
        if img is None or mask is None:
            print('Error loading image and mask')
            print('image:', img_name)
            print('mask:', mask_name)
        if exmask_name:
            exmask = imread_channel(exmask_name)
        else:
            exmask = np.array([])
        self.fit_model( img, mask, exmask )

    def fit_model(self, img, mask, exmask=np.array([]) ):
        ''' Do logistic regression to discriminate points in non-zero region of 
            mask from other points in mask and save estimated logistic regression parameters
            exmask: optionally exclude some pixels from image '''
        # Only need sklearn when we fit model parameters -- Do not require this for inference, see apply()
        from sklearn.linear_model import LogisticRegression
        data = img.reshape((-1,3)).astype(float)   # Reshape to N x 3
        label = (mask.ravel()>0).astype(int)       # Reshape to length N 
        if exmask.any():                    # Optionally exclude pixels
            keep = exmask.ravel()==0
            data = data[keep,:]
            label = label[keep]
        sk_logr = LogisticRegression(class_weight='balanced',solver='lbfgs')
        sk_logr.fit( data, label)
        self.cvec      = sk_logr.coef_                # Extract coefficients
        self.intercept = np.array(sk_logr.intercept_) # Extract intercept

    def print_params(self):
        print('Logist Regression params, cvec:',self.cvec,'intercept:',self.intercept)

    def apply(self, img):
        ''' Application of trained logisitic regression to an image
            img:         [MxNx3] input 3-channel color image
            prob_target: [MxN] output float probability that each pixel is on target
        '''
        # Lab 4: complete this function 
        return prob_target

    def find_largest_target(self, prob_target, threshold=0.5, minpix=20):
        # Lab 4: complete this function 
        return centroid, area, target_mask

    def find_all_targets(self, prob_target, threshold=0.5, minpix=20):
        # Lab 4: complete this function 
        return centroids, areas, target_mask
```
The main function is `fit_params` which take in an image, a mask, and an optional mask to exclude pixels (which is useful for pixels whose classification you do now know and so do not want to influence the fitting).  Each pixel is a 3-dimensional vector, and we reshape the data accordingly.  Then the `LogisticRegression` function from `sklearn` is used to fit a classifier that best separates the target pixels from background pixels.  The result of this is 2 terms: `cvec`, a 3-dimensional coefficient vector which on inference performs an inner product with the pixel vectors, and `intercept` which is simply an offset added to the result.  

One thing to note: I placed the line `from sklearn.linear_model import LogisticRegression` inside this fitting function.  That is because `sklearn` is a large library that installs on my Intel computer in a few seconds, but took more than an hour to install on the `Turtlebot` due to needing local compiling.  Our class will be used in two different cases: (1) training, which we are doing now, and (2) inference, when we use it on real images.  Only training needs this library, and so it makes sense to do this off-line.  The inference task is actually very simple and does not need this library, so by only importing this library in the fitting function, we can use the same class for inference on platforms that do not have the `sklearn LogisticRegression` utility installed.  

Speaking of inference, the function to do this is `apply`.  This inputs an image, passes each pixel through logistic regression followed by a sigmoid, and then outputs a probability that each pixel is a target pixel.  The actual implementation of this will be done as part of Lab 4.  Similarly the functions `find_largest_target` and `find_all_targets` will convert these pixel target probabilities into target hypotheses and is also part of Lab 4.

An analysis of the classification is done in function `plotClassification` shown here:
```python
def plotClassifcation( img, mask, pixProbs, threshold=0.5, savename='', outfolder=''):
    ''' Plot Classification Image Results, and output to files '''
    cv2.imshow("Train Image", img)
    cv2.imshow("Test Output", pixProbs )
    if mask.any():
        cv2.imshow("Train Mask", mask)

        #Create colored scoring image:
        TP = np.logical_and( mask > 0, pixProbs > threshold )   # green
        FP = np.logical_and( mask == 0, pixProbs > threshold )  # red
        FN = np.logical_and( mask > 0, pixProbs <= threshold )  # blue
        #add gray color if any of the above labels to reduce contrast slightly
        alabel = TP + FP + FN    
        # R,G,B classification for FP, TP, FN:
        eimg = np.stack( (FN, TP, FP), axis=2 ).astype(np.uint8) * 180 + 75 * alabel[:,:,None].astype(np.uint8)
        # Superimpose this on grayscale image:
        gimg = img.mean(axis=2).astype(np.uint8)
        combimg = (eimg * 3.0/5.0 + gimg[:,:,None] * 2.0/5.0).astype(np.uint8)
        cv2.imshow("Test Scoring", combimg)
    if outfolder:
        os.makedirs(args.outpath,exist_ok=True)
        cv2.imwrite(os.path.join(outfolder,'prob_'+savename), np.uint8(pixProbs*255) )
        if mask.any():
            cv2.imwrite(os.path.join(outfolder,'scoring_'+savename), combimg )
```
This plots the true positives, false positives and false negatives in separate colors and leaves the false negatives as the grayscale image.

## How Will Logistic Regression Be Used?

As part of Lab 4, you will be incorporating logistic regression into a ROS Node, which will enable you to do realtime colored-object detection from a camera feed.  This will be useful next week for extrinsic calibration as well as later on for line detection, sign detection and parking-spot detection.

## Can Detection Be Improved?

I was impressed by how well this very simple algorithm worked at detecting colored objects.  Nevertheless, it certainly has failures.  Portions of the sphere are incorrectly missed, while other pixels in the image are incorrectly classified as sphere pixels.  How might we do better?

The first thing to note is that our classifier is linear in RGB space.  This is a big limitation because an object's color pixel values depend not only on its own inherent color, but on the brightness of the illumination (as well as color of the illumination).  There are other color spaces such as `HSV` that separate the brightness or `value` from the `hue` and `saturation` which have greater invariance to incident illumination.  So one way to improve classification is to switch color spaces or add additional color-space values to each pixel.

Another significant limitation of our approach is that every pixel is classified independently of its neighbors.  It should be clear that using neighboring pixel information could improve the individual pixel classification.  And integrating spatial information is one of the key advantages of convolutional neural networks, which is a discussion for a later week.

___
### [Back to Index](../Readme.md)






